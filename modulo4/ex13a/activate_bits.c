#include "activate_bits.h"

int activate_bits(int a, int left, int right) {
	
	int maskLeft = -1; 	// '-1' apresenta todos os bits ativados
	left++;				// Incrementa para não ativar o bit 'left'
	int activeLeft = 0;
	if (left < 32) {
		activeLeft = maskLeft << left;
	}
	
	unsigned int maskRight = -1;	// Unsigned int para ser igual a SHR, caso contrário
	right = 32 - right;				// os bits que entravam na esquerda seriam 1 e não 0
	int activeRight = 0;
	if (right < 32) {
		activeRight = maskRight >> right;
	}

	return a|activeLeft|activeRight;

}
