#include <stdio.h>
#include "activate_bits.h"

int main() {
	
	int num = 0, left = 3, right = 29;
	
	printf("Número Original: %d\n", num);
	
	int res = activate_bits(num, left, right);
	printf("Número com os Bits ativados: %d\n", res);
	
	return 0;
}
