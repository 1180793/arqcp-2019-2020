#include <stdio.h>
#include "greater_date.h"

unsigned int build_date( int year , int month , int day ) {
	return  (year << 8)  | (month << 24 ) |  day ;
}

int main() {
	
	unsigned int date1 = build_date(2018, 12,27);
	unsigned int date2 = build_date(2018, 12,28);
	printf("Data 1: 2018/10/27\n");
	printf("Data 2: 2018/10/28\n");
	
	unsigned int res = greater_date(date1, date2);
	if (res == date2) {
		printf("Maior Data: 2018/10/28\n");
	}
	
	return 0;
}