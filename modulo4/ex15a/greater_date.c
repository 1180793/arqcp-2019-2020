#include "greater_date.h"

unsigned int greater_date(unsigned int date1, unsigned int date2) {
		
		unsigned int maskAno = 0x00FFFF00;
		unsigned int maskMes = 0x000000FF;
		unsigned int maskDia = 0xFF000000;
		
		unsigned int ano1 = date1 & maskAno;
		unsigned int ano2 = date2 & maskAno;
		if (ano1 == ano2) {
			unsigned int mes1 = date1 & maskMes;
			unsigned int mes2 = date2 & maskMes;
			if (mes1 == mes2) {
				unsigned int dia1 = date1 & maskDia;
				unsigned int dia2 = date2 & maskDia;
				if (dia1 > dia2) {
					return date1;
				} else {
					return date2;
				}
			} else if (mes1 > mes2) {
				return date1;
			} else {
				return date2;
			}
		} else if (ano1 > ano2) {
			return date1;
		} else {
			return date2;
		}
}
