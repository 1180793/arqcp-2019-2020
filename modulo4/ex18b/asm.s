.section .data
			
.section .text

	.global sum_multiples_x # int sum_multiples_x(char *vec, int x)
	
	
sum_multiples_x:


# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	pushl %esi
	movl $0, %ecx			# ecx=0
	movl 12(%ebp), %edx		# edx = x
	movl 8(%ebp), %esi		# esi = vec
	
ciclo:
	cmpb $0, (%esi)			# *vec==0?
	je fim					# if 0 jumps to end
	movl $0, %eax			# limpa eax
	movb (%esi), %al		# al=*vec
	idivb %dh				# ax/dh
	cmpb $0, %ah			# resto ==0?
	jne incrementa				# se resto!=0 vai para icrementa
	addb (%esi), %cl		# cl+=*vec
	
incrementa:
	incl %esi				# incrementa apontador
	jmp ciclo
	
fim:	
	movl %ecx, %eax			# move a soma para o retorno
 
	
	popl %esi

# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
	
	
		


