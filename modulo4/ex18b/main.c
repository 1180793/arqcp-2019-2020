#include <stdio.h>
#include "asm.h"

int main()
{
	char vetor[] = {2,1,4,3,4,3,1,5,7,0}; 
	char *vec= vetor;
	int x = 0x00000200;
	int sum = sum_multiples_x(vec, x);
	
	printf("x: %d (0x%08x) --> sum = %d\n", x, x, sum);
	
	return 0;
}
