#include <stdio.h>
#include "asm.h"
#include "asmvec.h"

int main(void) 
{
	int vec[] = {1,2,3,4,5,6,7,8,9,10};
	int num = 10;
	int *ptrvec = vec;
	
	changes_vec(ptrvec, num);
	
	return 0;
}
