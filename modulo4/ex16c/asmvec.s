.section .data

.section .text

.global changes_vec    		#void changes_vec(int *ptrvec, int num)

changes_vec:

	#prologue
	pushl %ebp      	#save previous stack frame pointer
	movl %esp, %ebp  	#the stack frame pointer for sum function

	movl 8(%ebp), %ecx		#move ptrvec para ecx   
	movl 12(%ebp), %edx		#move num para edx

ciclo:
	cmpl $0, %edx
	je end
	push %edx				#guarda num
	 
	push %ecx				#envia ecx
	call changes			#chama changes
	addl $4, %esp			#coloca a stack no lugar inicial

	popl %edx				#restora edx

	addl $4, %ecx			#avanca a posicao de ptrvec
	decl %edx
	jmp ciclo				#percorre o ciclo ate edx, num, chegar a 0

end:	
	#epilogue
	movl %ebp, %esp  	#restore the previous stack pointer ("clear" the stack)
	popl %ebp     		#restore the previous stack frame pointer
	ret
