.section .data
	.equ mascara1, 0x00800000	#constante que vai ser usada para verificar se os 4 bits menos significativos sao maiores que 7
	.equ mascara2, 0x00f00000	#constante que vai ser usada para trocar os 4 bits menos significativos	

.section .text

.global changes    		

changes:
	#prologue
	pushl %ebp      	#save previous stack frame pointer
	movl %esp, %ebp  	#the stack frame pointer for sum function
	
	movl 8(%ebp), %ecx		#move ptr para ecx
	movl (%ecx), %eax		#move o apontado de ecx para eax
	testl $mascara1, %eax	#verifica se os 4 bits menos significativos sao maiores que 7
	je fim					#se nao forem termina a execucao

	XORl $mascara2, %eax	#inverte os 4 bits menos significativos
	movl %eax, (%ecx)		#move eax para o valor apontado por ecx

fim:	

# epilogue
	movl %ebp, %esp  	# restore the previous stack pointer ("clear" the stack)
	popl %ebp     		# restore the previous stack frame pointer
	ret
