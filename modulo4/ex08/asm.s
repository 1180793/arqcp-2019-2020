.section .data
	
z:
	.int 0
	
.section .text

	.global calc 
		
calc:
	# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	movl 8(%ebp), %ecx 					#ecx -->a
	movl 12(%ebp), %edx 				#edx -->b
	
	pushl %edx
	movl (%edx), %edx					#edx = b
	sbb %ecx, %edx 						#edx = b-a
	movl %edx, z 						#z = b-a
	popl %edx	
	
	movl 16(%ebp), %ecx 				#ecx = c
	imull z, %ecx 						#ecx  = z*(b-a)
	
	sbb $2, %ecx 						#ecx = z*(b-a)-2
	
	movl %ecx, %eax 					#eax = z*(b-a)-2
	
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
