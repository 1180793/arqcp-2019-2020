#include <stdio.h>
#include "cube.h"

int main() {
	
	int x = 4;
	printf("%i^3 = %i\n",x, cube(x));
	
	return 0;
}
