.section .data

.section .text
	.global cube # int cube(int x)
	
cube:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	
	movl 8(%ebp), %eax	# x -> eax
	imull %eax, %eax	# eax = x^2
	movl 8(%ebp), %ecx	# x -> ecx
	imull %ecx, %eax	# eax * ecx = x^3

	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
