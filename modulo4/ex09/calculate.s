.section .data

.section .text
	.global calculate # int calculate(int a, int b)
	
calculate:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %ebx
	pushl %edi
	
	movl 8(%ebp), %edx		# a -> edx
	movl 12(%ebp), %ecx		# b -> ecx
	movl 12(%ebp), %eax
	addl %edx, %eax			# sum -> a + b
	
	# -- SOMA --
	# CALLER: Save Registers
	pushl %eax
	pushl %edx
	pushl %ecx

	pushl %eax 		# sum
	pushl %ecx 		# b
	pushl %edx		# a
	pushl $'+' 		# "+"
	
	call print_result # print_result(’+’, a, b, sum);

	addl $16 , %esp # 4 parametros

	# CALLER: Restore Registers
	popl %ecx
	popl %edx
	popl %eax
	
	# -- Multiplicacao --
	movl 12(%ebp), %ebx
	imull 8(%ebp), %ebx
	# CALLER: Save Registers
	pushl %eax
	pushl %edx
	pushl %ecx

	pushl %ebx 		# product
	pushl %ecx 		# b
	pushl %edx		# a
	pushl $'*' 		# "*"
	
	call print_result # print_result(’*’, a, b, product);

	addl $16 , %esp # 4 parametros

	# CALLER: Restore Registers
	popl %ecx
	popl %edx
	popl %eax
	
	subl %ebx, %eax
	
end:	
	popl %edi
	popl %ebx
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
	
