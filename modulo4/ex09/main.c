#include <stdio.h>
#include "calculate.h"

int main() {
	
	int a = 2, b = 8;
	printf("a = %d\n", a);
	printf("b = %d\n", b);
	
	int res = calculate(a, b);
	printf("(a + b) - (a * b) = %d\n", res);
	
	return 0;
}

void print_result(char op, int o1, int o2, int res) {
	printf("%d %c %d = %d\n", o1, op, o2, res);
}

