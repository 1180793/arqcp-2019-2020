.section .data
		
.section .text
	.global activate_bit 
activate_bit:

# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	subl $4 , %esp
	pushl %ebx
	
	movl $0, %eax			#deixa eax para o caso de nao mudar o bit

	movl 8(%ebp), %ecx		# move ptr para ecx
	
	movl (%ecx), %ebx		# move o apontado de ecx para ebx
	movl %ecx, -4(%ebp)		# guarda ecx na ebp

	movl $1, %edx			# inicializa o edx a 1
	movl 12(%ebp), %ecx		# move pos para ecx

	sall %cl, %edx			# move cl, edx bits para a esquerda

	ORl %edx, %ebx			# edx OR ebx

	movl -4(%ebp), %ecx		# retorna ecx ao valor inicial

	cmpl %ebx, (%ecx)		# compara ebx, resultado do OR, com ecx, o valor de ptr
	je nao_altera			# se forem iguais faz jmp para nao_altera 

	incl %eax				# se nao, passa eax para 1 para dar sinal que mudou o bit

	movl %ebx, (%ecx)		# move ebx para o valor apontado por ecx, ou ptr 

nao_altera:

# restore callee registers

	popl %ebx
	

# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
	
	
