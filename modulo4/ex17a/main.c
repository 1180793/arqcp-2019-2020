#include <stdio.h>
#include "add_byte.h"

int main() {
	
	int n = 5;
	int vec1[] = {n, 1, 21, 32, 0, 7};
	int vec2[n];
	int x = 6;
	
	printf("Valor X: %d\n", x);
	add_byte(x, (int *)vec1, (int *)vec2);
	
	printf("Vec Inicial: ");
	int i;
	for (i = 1; i < n + 1; i++) {
		printf("%d, ", *(vec1 + i));
	}
	
	printf("\nVec Final: ");
	for (i = 1; i < n + 1; i++) {
		printf("%d, ", *(vec2 + i));
	}
	
	printf("\n");
	
	return 0;
}