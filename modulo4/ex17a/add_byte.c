#include <stdio.h>
#include "add_byte.h"

void add_byte(char x, int *vec1, int *vec2) {
	
	int maskUltimoByte = 0x000000FF;
	int maskRestoBytes = 0xFFFFFF00;
	
	int n = *(vec1);
	*(vec2) = n;
	
	int i;
	for (i = 1; i < n + 1; i++) {
		int ultimoByte = *(vec1 + i) & maskUltimoByte; 	// Usa a mask e limpa tds os bytes exceto o ultimo
		ultimoByte = ultimoByte + x;					// Adiciona x ao ultimo byte
		ultimoByte = ultimoByte & maskUltimoByte;		// Limpa o resto dos bytes caso haja carry
		
		int restoBytes = *(vec1 + i) & maskRestoBytes;	// Usa a mask para limpar o ultimo byte
		*(vec2 + i) = restoBytes | ultimoByte;			// Junta o byte calculado com o resto dos bytes
	}
}
