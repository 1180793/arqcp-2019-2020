.section .data

.section .text

.global activate_2bits  

activate_2bits:
# prologue
	pushl %ebp      	# save previous stack frame pointer
	movl %esp, %ebp  	# the stack frame pointer for sum function


	movl 8(%ebp), %ecx		#move ptr para ecx
	movl 12(%ebp), %edx		#move pos para edx

	pushl %edx			#envia pos
	pushl %ecx			#envia ptr
	call activate_bit	#chama activate_bit, muda o bit no bit pos
	addl $8, %esp		#coloca a stack no lugar inicial

	movl $31, %eax		#coloca 31 no eax
	subl 12(%ebp), %eax		#subtrai pos ao eax


	pushl %eax			#envia a nova pos, eax
	pushl 8(%ebp)	    #envia ptr
	call activate_bit	#chama activate_bit, muda o bit na pos 31-pos
	addl $8, %esp		#coloca a stack no lugar inicial


# epilogue

	movl %ebp, %esp  	# restore the previous stack pointer ("clear" the stack)

	popl %ebp     		# restore the previous stack frame pointer

	ret
