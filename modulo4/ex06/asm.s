.section .data

.section .text
	.global test_equal
	
test_equal:
	# prologue
	pushl %ebp 					# save previous stack frame pointer
	movl %esp, %ebp 			# the stack frame pointer for sum function
	pushl %ebx
	
	movl 8(%ebp), %ebx			#ebx = a
	movl 12(%ebp), %edx 		#edx = b
	
	movl $1, %eax

ciclo: 
	movb (%ebx), %ch			#apontado de a para ch
	movb (%edx), %cl			#apontado de b para cl
	
	cmpw $0, %cx
	je fim						#se a e b estiverem vazios o programa acaba
	
	incl %ebx					#avanca apontador de a
	incl %edx					#avanca apontador de b
	
	cmpb %ch, %cl
	jne diferente

	jmp ciclo	

diferente:		
	movl $0, %eax
	
fim:
	# epilogue
	popl %ebx
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
