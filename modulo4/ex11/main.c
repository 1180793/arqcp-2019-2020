#include <stdio.h>
#include "rotate_left.h"
#include "rotate_right.h"

int main() {
	
	int num = 0b01011110;
	int nbits = 1;
	printf("Número Original: %d\n", num);
	
	int res = rotate_left(num, nbits);
	printf("Rotate Left: %d\n", res);
	
	res = rotate_right(num, nbits);
	printf("Rotate Right: %d\n", res);
	
	return 0;
}
