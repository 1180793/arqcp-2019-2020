#include "rotate_right.h"

int rotate_right(int num, int nbits) {
	
		return (num >> nbits | num << (32 - nbits));
		
}

// 	  num >> nbits(1)  	|		num << (8 - nbits)
//	0 1 0 1 1 1 1 0		|	  0 1 0 1 1 1 1 0 <
//	0 0 1 0 1 1 1 1 	|	> 0 0 0 0 0 0 0 0

//		0 0 1 0 1 1 1 1
// OR	0 0 0 0 0 0 0 0
// ---------------------
//		0 0 1 0 1 1 1 1