.section .data
	.equ maskAno, 0x00FFFF00
	.equ maskMes, 0x000000FF
	.equ maskDia, 0xFF000000
	
.section .text
	.global greater_date # unsigned int greater_date(unsigned int date1, unsigned int date2)
	
greater_date:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %ebx
	pushl %edi
	pushl %esi
	
	movl 8(%ebp), %edx 		# date1 -> edx
	movl 12(%ebp), %ebx 	# date2 -> ebx
	
	# Comparacao Ano
	movl $maskAno, %edi
	and %edx, %edi		# edi = anoDate1
	movl $maskAno, %esi
	and %ebx, %esi		# esi = anoDate2
	cmpl %edi, %esi
	jg greaterDate2
	jl greaterDate1
	
	# Comparacao Mes
	movl $maskMes, %edi
	and %edx, %edi		# edi = mesDate1
	movl $maskMes, %esi
	and %ebx, %esi		# esi = mesDate2
	cmpl %edi, %esi
	jg greaterDate2
	jl greaterDate1
	
	# Comparacao Dia
	movl $maskDia, %edi
	and %edx, %edi		# edi = diaDate1
	movl $maskDia, %esi
	and %ebx, %esi		# esi = diaDate2
	cmpl %edi, %esi
	jg greaterDate2
	jle greaterDate1
	
	
greaterDate1:
	movl 8(%ebp), %eax		# date1 > date2
	jmp end
	
greaterDate2:
	movl 12(%ebp), %eax		# date2 >= date1
	jmp end

end:
	popl %esi
	popl %edi
	popl %ebx
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
