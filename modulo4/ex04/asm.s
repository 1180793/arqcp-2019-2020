.section .data

.section .text
	.global sum_smaller
	
sum_smaller:
	# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	movl 8(%ebp), %eax			#eax = a
	movl 12(%ebp), %ecx 		#ecx = b
	movl 16(%ebp), %edx			#move smaller para edx
	movl %eax, (%edx)
		
	cmpl %ecx, %eax 			#a < b?
	jge AmaiorIgual
	addl %ecx, %eax 			#a + b
	jmp fim
	
AmaiorIgual:
	movl %ecx, (%edx)
	addl %ecx, %eax 			#a + b
	
	
fim:
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
