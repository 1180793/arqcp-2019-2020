#include <stdio.h>
#include "activate_bit.h"

int main()
{
	int x = 1;
	int *ptr = &x;
	int pos = 12;
	int a = x;
	int ret= activate_bit(ptr, pos);
	
	if(ret==-1)
	{
		printf("Valor dado para a posição do bit, %i, não é válido\n", pos);
	}
	if(ret==1)
	{
		printf("O bit %i do número %i foi mudado para 1\n", pos, a);
	}
	if(ret==0)
	{
		printf("O bit %i do número %i não foi mudado, pois já era 1\n", pos, a);
	}
	return 0;
}
