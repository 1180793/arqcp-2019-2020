.section .data

.section .text
	.global sum_n
	
sum_n:
	# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	
	movl 8(%ebp), %ecx	#move o n para ecx
	movl $0, %eax		#inicializa eax a 0
	movl $1, %edx		#inicializa edx a 1, edx vai funcionar como contador
	
	cmpl $0, %ecx		#ecx=<0?
	jle fim				#se sim, programa termina

ciclo:
	cmpl %ecx, %edx		#edx>numero passado por parâmetro?
	jg fim				#se sim, programa termina
	
	addl %edx, %eax		#adiciona edx ao eax
	incl %edx			#incrementa o edx
	jmp ciclo
	
fim:
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
