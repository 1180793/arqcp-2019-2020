#include <stdio.h>
#include "count_even.h"

int main() {
	
	short vector[]={1,2,-3,45,32,13,0,-12};
	int n = 8, i;	
	short *vec = vector;
	
	printf("VECTOR: ");
	for (i = 0; i < n; i++) {
		printf("%d, ", *(vector + i));
	}
	
	int res = count_even(vec, n);
	printf("\nQuantidade de Números Pares: %d\n", res);
	
	return 0;
}
