.section .data

.section .text
	.global count_even # int count_even(short *vec, int n)
	
count_even:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %ebx
	pushl %edi
	
	movl 8(%ebp), %edx	# *vec -> edx
	movl $0, %ecx		# contadorVec -> ecx
	movl $0, %eax		# contPar -> eax
	
	vec_loop:
		cmpl 12(%ebp), %ecx
		je end
		
		movw (%edx, %ecx, 2), %bx
		incl %ecx
		shr %bx
		movl $0, %edi
		adcl $0, %edi	
		cmpl $0, %edi	# verifica se existe carry(impar), ou nao(par)
		jne vec_loop
		incl %eax		# aumenta o contador no caso do numero ser par
	jmp vec_loop
	
end:	
	popl %edi
	popl %ebx
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
