#include <stdio.h>
#include "activate_bits.h"

int main() {
	
	int num = 3, left = 8, right = 18;
	
	printf("Número Original: %d\n", num);
	
	int res = activate_bits(num, left, right);
	printf("Número com os Bits ativados: %d\n", res);
	
	return 0;
}
