.section .data
		
.section .text
	.global add_byte # void add_byte(char x, int *vec1, int *vec2)
	
add_byte:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	subl $4 , % esp
	pushl %ebx
	pushl %edi
	pushl %esi
	
	movl 12(%ebp), %edi 	# edi = vec1
	movl 16(%ebp), %esi 	# esi = vec2
	
	movl (%edi), %eax		# n = vec1[0]
	movl %eax, (%esi) 		# vec2[0] = n
	
	incl %eax 				# n++ (n elementos de 1 até n + 1)
	movl %eax, -4(%ebp)		# -4(%ebp) = n
	
	movl $1, %eax 			# contador loop = 1

vec_loop:	
	cmpl -4(%ebp), %eax
	jge end
	
	movl (%edi,%eax,4), %edx 	# edx = *(vec1 + i)
	and $0x000000FF, %edx 		# *(vec1 + i) & maskUltimoByte
	addl 8(%ebp), %edx 			# ultimoByte + x
	and $0x000000FF, %edx  		# (ultimoByte + x) & maskUltimoByte para limpar o resto dos bytes caso a soma tenha excedido o 1º byte
	
	movl (%edi,%eax,4), %ecx 	# ecx = *(vec1 + i)
	and $0xFFFFFF00, %ecx 		# *(vec1 + i) & maskRestoBytes
	or %ecx, %edx 				# valorFinal = ultimoByte | restoBytes
	
	movl %edx, 	 (%esi,%eax,4) 	# *(vec2 + i) = valorFinal
	
	incl %eax
	jmp vec_loop

end:
	popl %esi
	popl %edi
	popl %ebx
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
	