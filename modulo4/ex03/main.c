#include <stdio.h>
#include "greatest.h"

int main() {
	
	int a = 4, b = -5, c = 7;
	printf("O maior número no conjunto {%d,%d,%d} é: %d\n", a, b, c, greatest(a,b,c));
	
	return 0;
}
