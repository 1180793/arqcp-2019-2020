.section .data

.section .text
	.global greatest # int greatest(int a, int b, int c)
	
greatest:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	
	movl 8(%ebp), %edx	# a -> edx
	movl 12(%ebp), %ecx	# b -> ecx
	cmpl %ecx, %edx
	jge jmp_aMaiorB
	jl jmp_bMaiorA
	
	jmp_aMaiorB:
		movl %edx, %eax		# a -> eax
		movl 16(%ebp), %ecx	# c -> ecx
		cmpl %eax, %ecx
		jge jmp_cMaior
		jl end
	
	jmp_bMaiorA:
		movl %ecx, %eax		# b -> eax
		movl 16(%ebp), %ecx	# c -> ecx
		cmpl %eax, %ecx
		jge jmp_cMaior
		jl end
		
	jmp_cMaior:
		movl 16(%ebp), %eax
		jmp end
end:
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
