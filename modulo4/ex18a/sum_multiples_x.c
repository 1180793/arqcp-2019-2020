#include <stdio.h>
#include "sum_multiples_x.h"

#define numBitsByte 8
int sum_multiples_x(char *vec, int x){
	
{
	int soma = 0;
	
	char multiplo = (x >> numBitsByte);
	
	while(*vec != 0)
	{
		if (*vec % multiplo== 0)
		{
			soma += *vec;
		}
		vec++;
	}
	
	return soma;
}
	
}

