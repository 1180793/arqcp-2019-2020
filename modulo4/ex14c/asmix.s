.section .data
		
.section .text

	.global mixed_sum 
	
mixed_sum:

# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	pushl 16(%ebp)		#envia pos
	pushl 12(%ebp)		#envia b
	pushl 8(%ebp)		#envia a
	call join_bits		#chama join_bits com a ordem a,b,pos
	addl $12, %esp		#coloca a stack no lugar inicial	

	movl %eax, %ecx		#move o retorno da primeira chamada para ecx
	push %ecx			#guarda o retorno da primeira chamada

	pushl 16(%ebp)		#envia pos
	pushl 8(%ebp)		#envia a
	pushl 12(%ebp)		#envia b
	call join_bits		#chama join_bits com a ordem b,a,pos
	addl $12, %esp		#coloca a stack no lugar inicial		

	popl %ecx
	addl %ecx, %eax		# adds ecx to eax, o retorno anterior soma ao mais recente
	
# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
