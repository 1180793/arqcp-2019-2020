.section .data
		
.section .text

	.global join_bits # int join_bits(int a, int b, int pos)
	
	
join_bits:


# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	pushl %ebx
	
	movl 8(%ebp), %eax		#move a para eax
	movl 12(%ebp), %edx		#move b para edx
	movl 16(%ebp), %ecx		#move pos para ecx
	
	cmp $31, %ecx			#verifica se pos ja chegou a 31
	jge fim					#se chegou termina a execucao
	
	movl $0, %ebx			#se nao, coloca 0 no ebx
	notl %ebx				#inverte ebx
	
	incl %ecx				#avanca a posicao
	sall %cl, %ebx			#move o ebx para a posicao pos+1
	
	andl %ebx, %edx			#realiza a operacao and entre ebx e edx, b
	
	notl %ebx				#inverte ebx
	
	andl %ebx, %eax			#realiza a operacao ebx and eax, a
	orl %edx, %eax			#realiza a operacao edx or eax, b or a 
	
fim:
	popl %ebx

# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
