.section .data
	
.section .text

	.global count_bits_zero 
	
count_bits_zero:

# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	movl 8(%ebp), %ecx	#move x para ecx
	
	movl $0, %eax		#eax vai ser o contador de bits inativos
	movl $0, %edx		#edx vai ser um contador para verificar se chegou ao fim do x
	
ciclo:
	cmpl $32, %edx		#verifica se o numero chegou ao fim
	je fim				#se tiver chegado ao fim, termina a execucao
	sarl %ecx			#movimenta o bit para a direita
	jc cf_ativada		#verifica se a carry flag foi ativada
	incl %eax			#se nao tiver sido ativada incrementa o contador de bits inativos
	incl %edx			#avanca o contador
	jmp ciclo			#percorre novamente o ciclo

cf_ativada:
	incl %edx			#se a cf tiver sido ativada incrementa apenas o contador
	jmp ciclo			#percorre novamente o ciclo
	
fim:
	
	

# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
