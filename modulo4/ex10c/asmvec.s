.section .data

.section .text

	.global vec_count_bits_zero
	
vec_count_bits_zero:

# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	# guardar ebx, esi, edi, se usados
	pushl %ebx
	pushl %esi
	
	movl $0, %eax		#inicializa eax a 0
	
	movl 8(%ebp), %esi	#move ptr para esi
	movl 12(%ebp), %edx #move o num de elementos de ptr para edx
	movl $0, %ecx		#inicia ecx a 0		
	
ciclo:
	cmpl %edx, %ecx		#verifica se ptr chegou ao fim
	je fim				#se chegou ao fim termina a execucao
	
	pushl %eax 			# guardar eax
	pushl %ecx 			# guardar ecx
	pushl %edx 			# guardar edx
	
	pushl (%esi)			#save previous stack frame pointer
	call count_bits_zero	#chama a função que conta os bits inativos
	addl $4, %esp			#coloca a stack no lugar inicial
	
	popl %edx 			# restaurar edx
	popl %ecx 			# restaurar ecx
	popl %ebx 			# restaurar valor do eax para o ebx

	addl %ebx, %eax 	# acumular no eax 
	
	incl %ecx			#incrementar o ecx para a comparacao
	addl $4, %esi		#muda a posicao do ptr
	jmp ciclo			#percorre o ciclo novamente
	
fim:
	
	# restaurar ebx, esi, edi, se usados
	popl %esi
	popl %ebx

# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
