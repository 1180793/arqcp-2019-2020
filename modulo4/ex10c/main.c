#include <stdio.h>
#include "asmvec.h"
#include "asm.h"

int main()
{
	int vec[] = {1,2,3,4,5,6,7,8};
	int *ptr = vec;
	int num = 8;
	printf("Há %i bits inativos no vector \n",vec_count_bits_zero(ptr, num));
	return 0;
}

