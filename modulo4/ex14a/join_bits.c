#define mask(n)		(~((1 << (n+1)) - 1))
#define mask2(n)		(~(mask(n)))

int join_bits(int a, int b, int pos)
{
	int res=a;
	
	if(pos<31)
	{
		res = b & mask(pos);			//guarda os valores dos bits à esquerda da posicao pos do numero b
		res = res | (a&mask2(pos));		//guarda os valores dos bits à direita da posicao pos do numero a
	}
	
	res = res | (a&mask2(pos));
	
	return res;
}
