.section .data

.section .text
	.global inc_and_square # int inc_and_square(int *v1, int v2)
	
inc_and_square:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	movl 8(%ebp), %edx	# *v1 -> edx
	movl 12(%ebp), %eax	# v2 -> eax

	imull %eax, %eax 	# v2 * v2
	incl (%edx)			# v1 + 1
	
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
