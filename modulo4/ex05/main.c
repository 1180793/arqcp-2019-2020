#include <stdio.h>
#include "inc_and_square.h"

int main() {
	
	int v = -3, v2 = -7;
	int *v1 = &v;
	
	printf("v2 ^ 2: %d\n", inc_and_square(v1, v2));
	printf("*v1 + 1: %d\n", *v1);
	 
	return 0;
}
