int count_bits_zero(int x)
{
	int count = 0;	//contador de bits inativos
	int i;			//variavel usada para percorrer os bits de x
	for (i = 0; i < sizeof(int)*8; i++)
	{
		if ((x&1)!=1)		//verifica se a operacao logica, and, modifica o bit atual para 1
		{
			count++;		//se nao for 1, aumenta o contador de bits inativos
		}
		
		x = x>>1;			//avanca para o bit da direita
	}
	
	return count; 
}
