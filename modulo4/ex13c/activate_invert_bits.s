.section .data

.section .text
	.global activate_invert_bits # int activate_invert_bits(int a, int left, int right)
	
activate_invert_bits:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %ebx
	
	pushl 16(%ebp)	# 3o parametro - right
	pushl 12(%ebp)	# 2o parametro - left
	pushl 8(%ebp)	# 1o parametro - a
	call activate_bits
	addl $12 , %esp # limpar parametros da stack
	
	not %eax		# inverte o resultado obtido pela funcao activate_bits
	
end:
	popl %ebx
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
