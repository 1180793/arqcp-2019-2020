.section .data

.section .text
	.global activate_bits # int activate_bits(int a, int left, int right)
	
activate_bits:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %ebx
	
	movl 8(%ebp), %eax	# a -> eax
	
	
	movl $-1, %edx
	movl 12(%ebp), %ecx	# left -> ecx
	
	incl %ecx
	
	cmpl $32, %ecx		# Verifica se não dá shift aos 32 bits
	jge activateRight
	shl %cl, %edx 		# mask << left + 1
	or %edx, %eax 		# a | maskLeft
	
activateRight:
	movl $-1, %edx
	movl 16(%ebp), %ebx	# right -> ebx
	
	movl $32, %ecx
	subl %ebx, %ecx 	# ecx = 32 - right
	
	cmpl $32, %ecx		# Verifica se não dá shift aos 32 bits
	jge end
	shr %cl, %edx 		# mask >> 32 - right
	or %edx, %eax 		# a | maskRight

end:
	popl %ebx
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
