.section .data
	.global ptrvec
	.global num

.section .text
	.global vec_greater20

vec_greater20:
	# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %ebx
	pushl %edi
	
	movl $0, %eax		#limpa eax
	movl ptrvec, %ecx	#apontador ptrvec
	movl $0, %edx		#limpa edx

ciclo:	
	cmpl num, %edx		#apontador chegou ao fim?
	je fim
	
	movl (%ecx), %ebx
	movl 4(%ecx), %edi
	adcl %edi, %ebx
	
	cmpl $20, %ebx
	jg greater
	
	addl $4, %ecx 		#avanca apontador
	incl %edx			#incrementa posicao apontador
	
	jmp ciclo

greater:
	addl $4, %ecx		#avanca apontador
	incl %edx			#incrementa posicao apontador
	incl %eax			#incrementa numero de numeros maiores do q 20
	
	jmp ciclo
	
fim:
	# epilogue
	popl %edi
	popl %ebx
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
	
	
	
