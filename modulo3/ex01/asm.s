.section .data
	.global ptr1
	
.section .text
	.global zero_count 
	
zero_count:

	# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function

	movl $0, %eax
	movl ptr1, %ecx
	
ciclo:
	cmpb $0, (%ecx)
	je end
	cmpb $'0', (%ecx)
	jne avanca
	incl %eax

avanca:
	incl %ecx
	jmp ciclo

end:
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
