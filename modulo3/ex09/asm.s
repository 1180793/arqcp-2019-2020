.section .data
	.global ptrvec
	.global num
	.global x

.section .text
	.global vec_search 
	
vec_search:
	# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %ebx
		
	movl $0, %eax		#limpa eax
	movl ptrvec, %ecx	#apontador ptrvec
	movl $0, %edx		#limpa edx

ciclo:	
	
	cmpl num, %edx		#apontador chegou ao fim?
	je fim
	
	movl $0, %ebx		#limpa ebx
	movw (%ecx), %bx	#move o apontado de ptrvec para bx
	cmpw x, %bx			#verifica se x é igual ao apontado
	jne incrementa
	
	movl %ecx, %eax		#move o apontador para eax
	jmp fim

incrementa:
	addl $2, %ecx		#avança o ptrvec
	incl %edx			#avança o num
	jmp ciclo
	
fim:
	# epilogue
	popl %ebx
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
