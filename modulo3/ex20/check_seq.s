.section .data
	.global ptrvec
	.global num

.section .text
	.global check_seq # int check_seq(void)
	
check_seq:
	# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	#%edx - ptrvec
	movl $0, %eax
	movl $0 ,%ecx

firstCheck:
	movl (%edx), %eax #*prtvec
	movl 4(%edx), %ecx #*ptrvec+1
	cmpl %eax, %ecx
	jle jmp_no_seq

secondCheck:
	movl 4(%edx), %eax #*prtvec+1
	movl 8(%edx), %ecx #*ptrvec+2
	cmpl %eax, %ecx
	jle jmp_no_seq
	
jmp_has_seq:
	movl $1, %eax
	jmp end
	
jmp_no_seq:	
	movl $0, %eax
	jmp end

end:	
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
