.section .data
	.global ptrvec
	.global num

.section .text
	.global count_seq # int count_seq(void)
	
count_seq:
	# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	movl $0, %eax
 	movl $3, %edx
	cmpl num, %edx
	jg jmp_no_seq
	
	subl $2, num #num=num -2 --- numero maximo de sequencias
		
	movl ptrvec , %edx
	movl $0 ,%ecx
	
vec_loop:
	cmpl num, %ecx
	je end
	
	pushl %eax
	pushl %ecx
		call check_seq
		cmpl $1 , %eax
	popl %ecx
	popl %eax
	
	je jmp_has_seq

jmp_continue_loop:	
	incl %ecx
	addl $4 ,%edx
	jmp vec_loop
	

jmp_has_seq:
	incl %eax
	jmp jmp_continue_loop
	
jmp_no_seq:
	movl $0, %eax
	jmp end
	
end:
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
	