.section .data
	.global ptrvec
	.global num

.section .text
	.global vec_add_one # void vec_add_one(void)
	
	
vec_add_one:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	movl $0, %eax
	movl $0, %edx
	movl $0, %ecx
	
	movl ptrvec, %ecx 	#apontador -> ecx

array_loop:
	cmpl num, %edx
	je end
	
	movl (%ecx), %eax	#apontado -> eax
	addl $1, %eax		#adiciona 1 ao apontado
	movl %eax, (%ecx)	#eax -> apontado por ecx
	
	addl $4, %ecx		#avança o ptrvec
	incl %edx			#avança o index
	jmp array_loop
	
end:
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
