#include <stdio.h>
#include "asm.h"

int vec[] = {2,3,4,5,6};
int *ptrvec = vec;
int num = 5;

int main(void) {
	
	int i = 0;
	printf("INITIAL: ");
	for(i = 0; i < num; i++) {
		printf("%d" , *(ptrvec + i));
	}
	vec_add_one();
	printf("\nFINAL:   ");
	for(i = 0; i < num; i++) {
		printf("%d" , *(ptrvec + i));
	}
	printf("\n");
	
	return 0;
}