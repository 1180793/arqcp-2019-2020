.section .data
	.global ptr1
	.global ptr2
	.global ptr3
count:
	.int 0
	
.section .text
	.global str_cat # void str_cat(void)
	
	
str_cat:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %esi
	pushl %edi
	
	movl $0, %eax
	movl $0, %ecx
	movl $0, %edx
	movl ptr1, %esi
	movl ptr2, %edi
	movl ptr3, %ecx

loop_str1:
	movb (%esi), %al
	movb %al, (%ecx)
	cmpb $0, (%esi)
	je loop_str2
	incl %esi
	incl %ecx
	incl count
	cmpl $40, count
	jge loop_str2
	jmp loop_str1
	
loop_str2:
	movb (%edi), %al
	movb %al, (%ecx)
	cmpb $0, (%edi)
	je end
	incl %edi
	incl %ecx
	incl count
	cmpl $40, count
	jge end
	jmp loop_str2
	
end:
	# epilogue
	popl %edi
	popl %esi
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
