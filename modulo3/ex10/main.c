#include <stdio.h>
#include "asm.h"

char str1[] = "teste ";
char *ptr1 = str1;
char str2[] = "& teste2.";
char *ptr2 = str2;
char str3[] = "";
char *ptr3 = str3;

int main(void) {
	
	printf("STRING 1: '%s'\n", str1);
	printf("STRING 2: '%s'\n", str2);
	
	str_cat();
	
	printf("STRING 3: '%s'\n", ptr3);
	
	return 0;
}