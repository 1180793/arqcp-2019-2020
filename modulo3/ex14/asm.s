.section .data
	.global ptrvec
	.global num
	.global x

.section .text
	.global exists # int exists(void)
	.global vec_diff # int vec_diff(void)

vec_diff:
	#prologue
	pushl %ebp       # save previous stack frame pointer
	movl %esp, %ebp  # the stack frame pointer for sum function
	pushl %ebx
	pushl %edi
	
	movl $0, %eax
	movl num, %ecx
	cmpl $0, %ecx
	je end
	movl ptrvec, %edx # pointer
	
	movl $0, %edi	# counter

	loop_vec:
		movw (%edx), %bx
		movw %bx, (x)
		pushl %edx
		pushl %ecx
		call exists
		popl %ecx
		popl %edx
		
		cmpl $1, %eax
		je jmp_duplicated
		incl %edi
		
		jmp_duplicated:
			decl %ecx
			addl $2, %edx

		cmp $0, %ecx
		jne loop_vec

		movl %edi, %eax
		jmp end

end:
	# epilogue
	popl %edi
	popl %ebx
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
