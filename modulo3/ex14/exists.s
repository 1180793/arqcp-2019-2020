.section .data
	.global ptrvec
	.global num
	.global x

.section .text
	.global exists 	 # int exists(void)
	.global vec_diff # int vec_diff(void)

exists:
	#prologue
	pushl %ebp       # save previous stack frame pointer
	movl %esp, %ebp  # the stack frame pointer for sum function
	pushl %ebx
	pushl %edi
	
	movl $0, %eax
	movl $0, %ebx
	movl ptrvec, %edx
	movl num, %ecx
	cmpl $0, %ecx
	je end


loop:
	movw (%edx), %ax
	cmpw x, %ax
	jne jmp_not_equal
	incl %ebx
	jmp_not_equal:
		addl $2, %edx
	decl %ecx
	cmp $0, %ecx
	je loop_end	
	jmp loop


loop_end:
	movl $0, %eax
	cmpl $2, %ebx
	jge jmp_has_duplicate
	jmp end
	
jmp_has_duplicate:
	movl $1, %eax

end:
	popl %edi
	popl %ebx
	movl %ebp, %esp  #  restore the previous stack pointer ("clear" the stack)
	popl %ebp        #  restore the previous stack frame pointer
	ret
