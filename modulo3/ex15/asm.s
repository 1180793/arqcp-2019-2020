.section .data
	.global ptrvec
	.global num

.section .text
	.global sum_first_byte
	
sum_first_byte:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %ebx 
	
	movl ptrvec, %ecx		#apontador ptrvec para ecx
	movl $0, %eax 			#eax a 0
	movl $0, %edx 			#edx a 0
	
ciclo:
	cmpl %edx, num 			#ptrvec=num?
	je fim					
	
	movsxb (%ecx), %ebx
	addl %ebx, %eax
		
	addl $4, %ecx 			#ptr++
	incl %edx 				#incrementa edx
	jmp ciclo 				#volta a comparacao ptrvec=num?

fim:
	# epilogue
	popl %ebx
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
	
