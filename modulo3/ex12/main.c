#include <stdio.h>
#include "asm.h"

short int vec[] = {0, -1, 2, 1, 100, 99, 101, 1000, 2000, -2000};
int num = 10;
short int *ptrvec = vec;

int main(void) {
	
	int i = 0;
	printf("INITIAL: ");
	for(i = 0; i < num; i++) {
		printf("%d, " , *(ptrvec + i));
	}
	int nChanged = vec_zero();
	printf("\nFINAL:   ");
	for(i = 0; i < num; i++) {
		printf("%d, " , *(ptrvec + i));
	}
	printf("\nCHANGED: %d\n", nChanged);
	
	return 0;
}

