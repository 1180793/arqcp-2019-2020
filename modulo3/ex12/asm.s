.section .data
	.global ptrvec
	.global num

.section .text
	.global vec_zero # int vec_zero(void)
	
	
vec_zero:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %ebx
	
	movl $0, %eax
	movl $0, %edx
	movl $0, %ecx
	movl $0, %ebx
	
	movl num, %ecx
	cmpl $0, %ecx
	je end
	
	movl ptrvec, %edx

vec_loop:
	cmpl $0, %ecx
	je end
	
	cmpw $100, (%edx)
	jge jmp_vec_change
	addl $2, %edx
	decl %ecx
	jmp vec_loop
	
jmp_vec_change:
	movw $0, (%edx)
	incl %eax
	addl $2, %edx
	decl %ecx
	jmp vec_loop
	

			
end:
	# epilogue
	popl %ebx
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
