.section .data
	.global ptr1
	.global ptr2
	.global num

.section .text
	.global swap # void swap(void)

swap:
	#prologue
	pushl %ebp       # save previous stack frame pointer
	movl %esp, %ebp  # the stack frame pointer for sum function
	pushl %ebx
	pushl %esi
	
	movl $0, %eax
	movl $0, %ecx
	cmpl num, %ecx
	je end
	movl ptr1, %edx # pointer 1
	movl ptr2, %esi # pointer 2

	loop_vec:
		cmpl num, %ecx
		je end
		movb (%edx), %al
		movb (%esi), %bl
		movb %al, (%esi)
		movb %bl, (%edx)
		incl %edx
		incl %esi
		incl %ecx
		jmp loop_vec

end:
	# epilogue
	popl %esi
	popl %ebx
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
