#include <stdio.h>
#include "asm.h"

char  * ptr1;  
char  * ptr2;  
int num; 

int main(void) {
	
	char vec1[] = "Amanha eu estou aqui";
	char vec2[] = "O Miguel esta doente";
	
	ptr1 = vec1;
	ptr2 = vec2;
	num = 20;
	
	printf("\nINITIAL VEC1: %s", vec1);
	printf("\nINITIAL VEC2: %s\n", vec2);
	
	swap();
	
	printf("\nFINAL VEC1: %s", ptr1);
	printf("\nFINAL VEC2: %s\n", ptr2);
	
	return 0;
}
