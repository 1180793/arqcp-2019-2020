#include <stdio.h>
#include "asm.h"

char vec[] = "Amanha a lua";
char *ptr1 = vec;
int num = 5;

int main(void) {
	
	printf("INITIAL: %s\n", vec);
	
	int nChanged = encrypt();
	
	printf("FINAL:   %s\n", vec);
	printf("CHANGED: %d\n", nChanged);
	
	return 0;
}