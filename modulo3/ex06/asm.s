.section .data
	.global ptr1
	.global num

.section .text
	.global encrypt # int encrypt(void)
	
	
encrypt:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	movl $0, %eax
	movl $0, %edx
	movl $0, %ecx
	
	movl ptr1, %ecx 	#apontador -> ecx

encrypt_loop:
	movb (%ecx), %dl
	
	cmpb $0, %dl
	je end
	cmpb $'a', %dl
	je increment_vec
	cmpb $' ', %dl
	je increment_vec
	
	addl $2, (%ecx)
	incl %eax
	
increment_vec:
	addb $1, %cl
	jmp encrypt_loop
	
end:
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
