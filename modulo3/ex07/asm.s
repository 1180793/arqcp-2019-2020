.section .data
	.global ptr1

.section .text

	.global encrypt 
	.global decrypt
	
	
encrypt:
	# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function

	movl ptr1 , %ecx 	#move o apontador da string para ecx

	movl $0, %eax 		#limpa o registo eax
	movb (%ecx), %dl	#move o apontado ptr1 para dl

	
verificacao:
	movb (%ecx) , %dl 	#move o apontado ptr1 para dl
	
	cmpb  $0, %dl 		#verifica se nao chegou ao fim
	 
	je fim 				# salta se chegou ao fim
	
	cmpb $'a', %dl 		#compara a com o char
	je incrementa
	
	cmpb  $' ', %dl 	#compara espaço com o char
	je incrementa
	
	addb $2, %dl 		#adiciona 2 se nao for a nem espaço
	
	incl %eax 			#incrementa o nr de alteraçoes
	
incrementa:
	movb %dl, (%ecx)	#move o apontado para ecx
	incl %ecx			#incrementa o apontador
	jmp verificacao
	
	
# ------------------------------------------------------------------
	
decrypt:
	# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function

	movl ptr1 , %ecx

	movl $0, %eax 		

	
verificacao1:
	movb (%ecx) , %dl 	
	
	cmpb  $0, %dl 		
	 
	je fim 				
	
	cmpb $'a', %dl 		
	je incrementa1
	
	cmpb  $' ', %dl 	
	je incrementa1
	
	subb $2, %dl 		
	
	incl %eax 			
	
incrementa1:
	movb %dl, (%ecx)	
	incl %ecx			
	jmp verificacao1


fim:

# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
