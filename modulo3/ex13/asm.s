.section .data
    .global ptrvec
    .global num

.section .text
    .global keep_positives 

keep_positives:
    # prologue
    pushl %ebp # save previous stack frame pointer
    movl %esp, %ebp # the stack frame pointer for sum function
    pushl %ebx

    movl ptrvec, %edx        # apontador ptrvec
    movl $0, %ecx            # contador

ciclo:
    cmpl num, %ecx            #apontador chegou ao fim?
    je fim                    #se chegou ao fim, termina o ciclo

    movl (%edx), %eax        #move o apontado de ptrvec para eax
    cmpl $0, %eax
    jl jmp_negativo

    addl $4, %edx            #avança o apontador
    incl %ecx                #avança index do apontador
    jmp ciclo                #volta a percorrer o ciclo

jmp_negativo:
    movl %ecx, (%edx)
    addl $4, %edx
    incl %ecx
    jmp ciclo

fim:
    # epilogue
    popl %ebx
    movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
    popl %ebp # restore the previous stack frame pointer
    ret
