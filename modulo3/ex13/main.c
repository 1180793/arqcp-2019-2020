#include <stdio.h>
#include "asm.h"

int vec[]= {-2,4,5,-3};
int num = 4;
int *ptrvec = vec;

int main (void) 
{
	printf("INITIAL VEC: ");
    int i;
    for(i = 0; i < num; i++){
        printf("%d, ", vec[i]);
    }
	keep_positives();
	printf("\nFINAL VEC:   ");
    for (i = 0; i < num; i++) {
        printf("%d, ", *(ptrvec+i));
    }
	printf("\n");
	return 0;
}
