.section .data
	.global ptrvec
	.global num

.section .text
	.global vec_sum 
	.global vec_avg
	
vec_sum:
	# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function

	movl ptrvec, %ecx		#apontador ptrvec para ecx
	movl $0, %eax 			#eax a 0
	movl $0, %edx 			#edx a 0
	
ciclo:
	cmpl %edx, num 			#ptrvec=num?
	je fim					
	
	addl (%ecx), %eax 		#eax = eax + ptrvec
		
	addl $4,%ecx 			#ptr++
	incl %edx 				#incrementa edxl
	jmp ciclo 				#volta a comparacao ptrvec=num?
	
#-----------------------------------------------------------------------------

vec_avg:
	# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	movl $0, %eax
	cmpl $0, num
	je fim
	
	call vec_sum
	
	movl $0, %edx			#limpa edx
	
	cdq
	idivl num
		

	
fim:	
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
