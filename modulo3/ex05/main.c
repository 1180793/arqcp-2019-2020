#include <stdio.h>
#include "asm.h"

int num = 5;
int vec[] = {5,5,10,5,5};
int *ptrvec = vec;
int soma = 0;
int media = 0;

int main(void) {
	soma = vec_sum();
	printf("Soma do vetor : %i\n", soma);
	media = vec_avg();
	printf("Media soma do vetor : %i\n", media);
	return 0;
}
