#include <stdio.h>
#include "asm.h"

int vec[] = {1,3,-5,3,4,20,2,0,8};
int *ptrvec = vec;  
int num = 9;

int main(void) {
	
	printf("INITIAL VEC: ");
	int i;
	for(i = 0; i < num; i++){
		printf("%d, ", vec[i]);
	}
	
	array_sort();
	printf("\nFINAL VEC:   ");
	for (i = 0; i < num; i++) {
		printf("%d, ", *(ptrvec + i));
	}
	printf("\n");
	
	return 0;
}
