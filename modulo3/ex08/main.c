#include <stdio.h>
#include "asm.h"

int vec[] = {1,2,3,4,5,6,7,8,9,12};
int num = 10;
int *ptrvec = vec;
int even = 0;

int main(void) {
	
	int i = 0;
	printf("ARRAY:    ");
	for(i = 0; i < num; i++) {
		printf("%d, " , *(ptrvec + i));
	}
	
	printf("\nEVEN SUM: %d\n", vec_sum_even());

	return 0;
}
