.section .data
	.global even

.section .text
	.global test_even # int test_even(void)
	
test_even:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	movl even, %eax
	movl $2, %ecx
	movl $0, %edx
	cdq
	idivl %ecx
	cmpl $0, %edx
	je jmp_even
	jne jmp_not_even
	
	jmp_even:
		movl $1 ,%eax
		jmp end
	
	jmp_not_even:
		movl $0 ,%eax
		jmp end
	
end:
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
