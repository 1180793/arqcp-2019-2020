.section .data
	.global ptrvec
	.global num
	.global even

.section .text
	.global vec_sum_even # int vec_sum_even(void)
	
vec_sum_even:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %ebx
	
	movl $0, %eax
	movl ptrvec, %edx
	movl $0, %ecx
	
	vec_loop:
		movl (%edx), %ebx
		movl %ebx, even
		cmpl num, %ecx
		je end
		pushl %eax
		pushl %ecx
		pushl %edx
			call test_even
		cmpl $0, %eax
		popl %edx
		popl %ecx
		popl %eax
		je jmp_skip_number
		addl (%edx), %eax
		jmp_skip_number:
			addl $4, %edx
			incl %ecx
		jmp vec_loop
	
end:
	# epilogue
	popl %ebx
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
