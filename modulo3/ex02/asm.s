.section .data
	.global ptr1
	.global ptr2

.section .text
	.global str_copy_porto # void str_copy_porto(void)
	
	
str_copy_porto:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	movl $0, %eax
	movl ptr1, %edx		#pointer ptr1
	movl ptr2, %ecx		#pointer ptr2

string_loop:
	movb (%edx), %al
	cmpb $'v', %al
	jne jmp_not_v
	movb $'b', %al

jmp_not_v:
	movb %al,(%ecx)
	cmpb $0, %al
	je end
	incl %ecx
	incl %edx
	jmp string_loop
	
end:
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
