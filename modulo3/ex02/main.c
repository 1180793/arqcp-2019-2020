#include <stdio.h>
#include "asm.h"

char str[] = "V v B b\n";
char *ptr1 = str;
char str2[] = "";
char *ptr2 = str2;

int main(void) {
	
	printf("INITIAL: %s", str);
	str_copy_porto();
	printf("FINAL:   %s", str2);
	
	return 0;
}