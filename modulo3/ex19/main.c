#include <stdio.h>
#include "asm.h"

char vec[] = {20,1,2,1,1,2,3,20,20,20,10,11};
int num = 12;
char *ptrgrades = vec;

int freq[21];
int *ptrfreq = freq;

int main(void) {
    int i;
    printf("Notas: ");
    for (i = 0; i < num; i++)
    {
        printf("%i, ", freq[i]);
    }
    printf("\n Frequência Absoluta: \n");
    frequencies();
    for (i = 0; i < 21; i++)
    {
        printf("NOTA %i:  %i\n",i,  freq[i]);
    }

    return 0;
}
