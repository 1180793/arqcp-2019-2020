.section .data
    .global ptrgrades
    .global ptrfreq
    .global num

.section .text
    .global frequencies

frequencies:
    # prologue
    pushl %ebp # save previous stack frame pointer
    movl %esp, %ebp # the stack frame pointer for sum function
    pushl %ebx

    movl ptrgrades , %eax
    movl ptrfreq , %edx
    movl $0 , %ecx
    movl $0 , %ebx

preencher:
    cmpl $21 , %ebx				#verifica se já chegou ao fim do vetor da frequencia absoluta
    je freqAbsoluta				#apos preencher vetor salta para proximo ciclo
    movl $0 , (%edx)			#preenche vetor a 0
    addl $4, %edx				#avança posição
    incl %ebx					#incrementa contador
    jmp preencher

freqAbsoluta:
    movl ptrfreq, %edx			#move ptrfreq para edx
    cmpl num , %ecx				#verifica se já chegou ao fim do vetor das notas
    je end
    movsxb (%eax), %ebx			#move valor da nota para ebx
    incl (%edx , %ebx, 4)		#aumenta em vecfreq o elemento com index = ebx
    incl %eax					#avança apontador
    incl %ecx					#incrementa contador
    jmp freqAbsoluta

end:
    # epilogue
    popl %ebx
    movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
    popl %ebp # restore the previous stack frame pointer
    ret
