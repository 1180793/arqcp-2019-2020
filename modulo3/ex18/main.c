#include <stdio.h>
#include "asm.h"
#include "sort.h"

int vec[] = {2,1,1,1,1,1,1,1,1};
int num = 9;
int vecFinal[9];

int *ptrsrc = vec;
int *ptrdest = vecFinal;

int main(void) {
	
	printf("INITIAL VEC: ");
	int i;
	for(i = 0; i < num; i++){
		printf("%d, ", vec[i]);
	}
	
	int n = sort_without_reps();
	
	printf("\nFINAL VEC:   ");
	for (i = 0; i < n; i++) {
		printf("%d, ", vecFinal[i]);
	}
	
	printf("\nN: %d\n", n);
	 
	return 0;
}
