.section .text
	.global exists # int exists(void)
	# checks if the number pointed by (%ecx) exists in the array %edx
	
exists:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %ebx
	pushl %edi
	pushl %esi
	
	movl $0, %edi # counter
	cmpl $0, %eax #	ptrdest size
	je jmp_add_dest
	
	movl (%ecx), %ecx # ecx = *ptrsrc


vec_loop:
	cmpl %edi, %eax 	# counter = ptrdest
	je jmp_add_dest
	
	movl (%edx), %ebx 	# ebx = *ptrdest
	cmpl %ebx, %ecx 	# *ptrdest = *ptrsrc?
	je jmp_skip
	
	addl $4, %edx		# ptrdest++
	incl %edi 			# counter++
	
	jmp vec_loop


jmp_skip:
	movl $1, %eax # number already exists in new array
	jmp end
	
jmp_add_dest:
	movl $0, %eax # number doesn't exist in new array
	jmp end

end:
	# epilogue
	popl %esi
	popl %edi
	popl %ebx
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
