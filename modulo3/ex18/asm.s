.section .data
    .global ptrsrc 
    .global ptrdest
    .global num
	
.section .text
    .global sort_without_reps # int sort_without_reps(void)

sort_without_reps:
    # prologue
    pushl %ebp 		# save previous stack frame pointer
    movl %esp, %ebp # the stack frame pointer for sum function
	pushl %ebx
	pushl %edi
	pushl %esi
	
	
	movl $0, %edi # nVerified
	movl $0, %eax # nNewArray

    movl ptrsrc , %ecx #ecx = ptrsrc
    movl ptrdest, %edx #edx = ptrdest

	pushl %eax
	pushl %ecx
	pushl %edx
		call sort # order ptrsrc ascending
	popl %edx
	popl %ecx
	popl %eax

vec_loop:
	cmpl num, %edi
	jge end 
	pushl %eax
	pushl %ecx
	pushl %edx
		call exists # Duplicated Number? If return 1 skip number, if return 0 add number to new vector
	cmpl $1, %eax
	popl %edx
	popl %ecx
	popl %eax
	je jmp_skip
	jne jmp_add_to_dest
	
jmp_add_to_dest:
	movl (%ecx), %esi 			# esi = *ptrsrc
	movl %esi , (%edx,%eax,4) 	# *ptrsrc -> *(ptrdest + eax)
	incl %eax 					# nNewArray++
	
jmp_skip:
	addl $4, %ecx 	# ptrvec++
	incl %edi 		# nVerified++
	jmp vec_loop
	
end:
	# epilogue
	popl %esi
	popl %edi
	popl %ebx
    movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
    popl %ebp 		# restore the previous stack frame pointer
    ret
