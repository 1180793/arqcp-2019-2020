.section .data
	.global ptrsrc
	.global num
	
.section .text
	.global sort # void sort (void)
	# orders array pointed by ptrsrc
	
sort:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %ebx
	pushl %esi
	pushl %edi

	decl num

loop_initial:
	movl $0, %edx		# counter
	movl $0, %eax 		# "Order Flag" - isOrdered: 0, hasChanged: 1
	movl ptrsrc, %ecx	# ecx to start of ptrsrc

vec_loop:	
	cmpl num, %edx
	jge jmp_end_loop
	
	movl (%ecx), %ebx
	movl 4(%ecx), %edi
	cmpl %edi, %ebx
	jle jmp_ascending
		movl %ebx, 4(%ecx)
		movl %edi, (%ecx)
		movl $1 ,%eax	
	
	jmp_ascending:
		addl $4, %ecx
		incl %edx
		jmp vec_loop
	
jmp_end_loop:
	cmpl $0, %eax
	je end
	jne loop_initial
		
end:		
	incl num
	# epilogue
	popl %edi
	popl %esi
	popl %ebx
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
