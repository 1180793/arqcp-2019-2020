#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cifra.h"

int main(void) {

	// TESTE DECIFRA_PAR
	char vec1 = 'a';
	char vec2 = 'a';

	short numero1 = 120, chave1 = 50;
	
	char* ptr1 = &vec1;
	char* ptr2 = &vec2;
	
	decifra_par(numero1, chave1, ptr1, ptr2);
	
	printf("C1: %c\n" , vec1);
	printf("C2: %c\n" , vec2);
	
	// --------------------------------------------------------
	
	int nMensagens = 2;
	Cifra* vec = (Cifra*) malloc(sizeof(Cifra) * nMensagens);
	
	vec->tamanho_cifrado = 7;
	vec->texto_cifrado = (short*) calloc(7, sizeof(short));
	vec->texto_original = (char *) calloc(7 * 2 + 1, sizeof(char));
	
	*(vec->texto_cifrado + 0) = 12105;
    *(vec->texto_cifrado + 1) = 2136;
	*(vec->texto_cifrado + 2) = 7692;
	*(vec->texto_cifrado + 3) = 8009;
	*(vec->texto_cifrado + 4) = 23405;
    *(vec->texto_cifrado + 5) = 10621;
	*(vec->texto_cifrado + 6) = 14460;
	
	short chave = 31532;
	decifra_string(vec, chave);
	printf("Texto Original 1: %s \n", vec->texto_original);
	
	(vec + 1)->tamanho_cifrado = 7;
	(vec + 1)->texto_cifrado = (short *) calloc(7, sizeof(short));
	(vec + 1)->texto_original = (char *) calloc(7 * 2 + 1, sizeof(char));
	
	*((vec + 1)->texto_cifrado + 0) = 15956;
    *((vec + 1)->texto_cifrado + 1) = 6721;
	*((vec + 1)->texto_cifrado + 2) = 7692;
	*((vec + 1)->texto_cifrado + 3) = 8009;
	*((vec + 1)->texto_cifrado + 4) = 23405;
    *((vec + 1)->texto_cifrado + 5) = 10621;
	*((vec + 1)->texto_cifrado + 6) = 14460;
	
	chave = 31532;
	decifra_string((vec + 1), chave);
	printf("Texto Original 2: %s \n", (vec + 1)->texto_original);
	
	int i;
	for (i = 0; i < nMensagens; i++) {
		free((vec + i)->texto_cifrado);		// Liberta o espaço reservado para o texto cifrado da struct Cifra na posição vec[i]
		free((vec + i)->texto_original);	// Liberta o espaço reservado para o texto original da struct Cifra na posição vec[i]
	}
	free(vec);	// Liberta o espaço reservado para o vetor que guarda as structs Cifra
	
	return 0;		
}