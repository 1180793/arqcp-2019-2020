.section .data
	.equ CIFRA_TEXTO_CIFRADO_OFFSET, 0
	.equ CIFRA_TAMANHO_CIFRADO_OFFSET, 4
	.equ CIFRA_TEXTO_ORIGINAL_OFFSET, 8
	
.section .text
	.global decifra_string # void decifra_string(Cifra *c, short chave)

decifra_string:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	subl $12 , %esp
	pushl %ebx			#
	pushl %edi			# CALLEE SAVES
	pushl %esi			# 
	
	movl 8(%ebp), %esi	# esi = cifra
	movw 12(%ebp), %bx	# ebx = chave
	movw %bx, -4(%ebp)	# -4(%ebp) = chave
	
	movl $0, %ecx									# ecx = contador
	movl CIFRA_TEXTO_CIFRADO_OFFSET(%esi), %edx		# edx = apontador texto cifrado
	movl CIFRA_TEXTO_ORIGINAL_OFFSET(%esi), %edi	# edi = apontador texto original
	movl $0, %ebx
	
	cifra_loop:
		cmpl CIFRA_TAMANHO_CIFRADO_OFFSET(%esi), %ecx		# verifica se o loop acabou
		je end
		
		movl (%edx, %ecx, 2), %eax				# numero (1º parametro)
		
		pushl %eax
			leal (%edi, %ebx, 1), %eax			# endereco c1 -> eax
			movl %eax, -8(%ebp)					# eax -> -8(%ebp)
			incl %ebx							# ebx + 1 para dar o endereço do char seguinte
			leal (%edi, %ebx, 1), %eax			# endereco c2 -> eax
			movl %eax, -12(%ebp)				# eax -> -12(%ebp)
			incl %ebx							# ebx + 1 para dar o endereço do char seguinte
		popl %eax
		
		pushl %eax				#
		pushl %ecx				# CALLER SAVES
		pushl %edx				#
			
			pushl -12(%ebp)	# c2 - 4 parametro
			pushl -8(%ebp) 	# c1 - 3 parametro
			pushl -4(%ebp)	# chave - 2 parametro
			pushl %eax		# numero - 1 parametro 
				call decifra_par
			addl $16 , %esp	# limpa os parametros da stack 4 * nr de parametros = 4*4 = 16
		
		popl %edx				#
		popl %ecx				# CALLER RESTORES
		popl %eax				# 
		
		
		incl %ecx				# incrementa ecx
	jmp cifra_loop				# volta para o inicio do loop
	
	movl $0, (%edi, %ebx, 1)	# adiciona 0 a string para terminar
	
end:
	popl %esi					#
	popl %edi					# CALLE RESTORES
	popl %ebx					#
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
	