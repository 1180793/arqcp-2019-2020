.section .data
	
.section .text
	.global decifra_par # void decifra_par(short numero, short chave, char *c1, char *c2).

decifra_par:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	push %edi
	push %esi
	
	movw 8(%ebp), %dx		# numero
	movw 12(%ebp), %ax		# chave
	movl 16(%ebp), %esi		# c1
	movl 20(%ebp), %edi		# c2
	
	xorw %dx, %ax			# short que representa um par de caracteres, pela ordem inversa à que devem aparecer no texto original
	movb %al, (%edi)		# menos significativo para c2
	movb %ah, (%esi)		# mais significativo para c1
	
end:
	popl %esi
	popl %edi
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
	