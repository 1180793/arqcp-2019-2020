.section .data
	.global A	#char - 1 byte
	.global B	#short int - 2 bytes
	.global C	#int - 4 bytes
	.global D	#int - 4 bytes

.section .text
	.global sum_and_subtract
	
sum_and_subtract:
	#prologue
	pushl %ebp		# save previous stack frame pointer
	movl %esp, %ebp	# the stack frame pointer for sum function
	pushl %ebx
	pushl %edi
	pushl %esi
	
	movl $0, %eax
	movl $0, %edx
	
	movl C, %eax
	cdq             # C -> EDX:EAX
	movl %eax, %esi
	movl %edx, %edi
	movl %esi, %ecx  # ! LOW
	movl %edi, %ebx  # ! HIGH
	
	movsxb A, %eax
	cdq              # A -> EDX:EAX
	movl %eax, %esi
	movl %edx, %edi
	addl %esi, %ecx
	adcl %edi, %ebx
	
	movl D, %eax
	cdq
	movl %eax, %esi
	movl %edx, %edi
	subl %esi, %ecx
	sbbl %edi, %ebx
	
	movsxw B, %eax
	cdq
	movl %eax, %esi
	movl %edx, %edi
	addl %esi, %ecx
	adcl %edi, %ebx
	
	movl %ecx, %eax
	movl %ebx, %edx
	
	# epilogue
	popl %esi
	popl %edi
	popl %ebx
	movl %ebp, %esp	# restore the previous stack pointer ("clear" the stack)
	popl %ebp		# restore the previous stack frame pointer
	ret
