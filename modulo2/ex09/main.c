#include <stdio.h>
#include "sum_and_subtract.h"

char A = 0x06;	//6
short int B = 0x0010;	//16
int C = 0x0000000C;	//12
int D = 0x1;	// 1

int main(void){
	
	long long result = sum_and_subtract();
	printf("%d + %d - %d + %d = %lld\n", C, A, D, B, result);
	
	return 0;
}
