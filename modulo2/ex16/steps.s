.section .data
	.global num
.section .text
	.global steps
	
steps:
	#prologue
	pushl %ebp
	movl %esp, %ebp #the stack frame pointer for sum function
	
	movl $0,%edx
	movl num, %eax
	movl $3, %ecx	
	mull %ecx
	addl $6,%eax
	adcl $0,%edx
	movl $3,%ecx
	cdq
	idiv %ecx
	movl $0, %edx
	addl $12, %eax
	adcl $0, %edx
	subl num, %eax
	subl $1, %eax
	
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
