.section .data
	.global current
	.global desired

.section .text
	.global needed_time	# int needed_time(void)

needed_time:
	# prologue
	pushl %ebp		# save previous stack frame pointer
	movl %esp, %ebp	# the stack frame pointer for sum function

	movl $0, %ecx
	movw current , %cx
	movl $0, %edx
	movw desired , %dx
	movl $0, %eax
	
	cmpw %cx, %dx 			# dx - cx
	je end
	jg jmp_increase_temp	# dx > cx
	jl jmp_decrease_temp	# dx < cx
	
jmp_increase_temp:
	subw %cx, %dx
	movl %edx, %eax
	movl $120, %ecx
	imull %ecx
	jmp end
	
jmp_decrease_temp:
	subw %dx, %cx 
	movl %ecx, %eax
	movl $180, %ecx
	imull %ecx
	jmp end


end:
	# epilogue
	movl %ebp, %esp	# restore the previous stack pointer ("clear" the stack)
	popl %ebp		# restore the previous stack frame pointer
	ret
	