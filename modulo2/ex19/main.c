#include <stdio.h>
#include "needed_time.h"

short current = 0, desired = 0;
int time = 0;

int main(void) {
	
	current = 15;
	desired = 5;
	
	time = needed_time();
	printf("%hdºC -> %hdºC : %d seconds\n", current, desired, time);
	
	return 0;
}
