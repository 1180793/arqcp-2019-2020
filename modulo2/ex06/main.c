#include <stdio.h>
#include "concatBytes.h"

char byte1 = 'd', byte2 = 'D';

int main(void) {
	
	printf("Byte1: %c\n", byte1);
	printf("Byte2: %c\n", byte2);
	
	short int s = concatBytes();
	printf("Concatenated Value: 0x%04X\n", s);
	
	return 0;
}
