.section .data
	.global op1
	.global op2
	.equ CONST, 15

.section .text
	.global sum_v2	# int sum_v2(void)

sum_v2:
	# prologue
	pushl %ebp		# save previous stack frame pointer
	movl %esp, %ebp	# the stack frame pointer for sum function
	
	movl $CONST, %eax
	sub op1, %eax
	
	movl $CONST, %ecx
	sub op2, %ecx
	
	subl %ecx, %eax
	
	# epilogue
	movl %ebp, %esp  #  restore the previous stack pointer ("clear" the stack)
	popl %ebp     #  restore the previous stack frame pointer
	ret
