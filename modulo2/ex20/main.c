#include <stdio.h>
#include "check_num.h"

int num = 0, res = 0;

int main(void) {
	
	num = -20;
	printf("check(%d) = %d\n", num, check_num());
	
	num = -15;
	printf("check(%d) = %d\n", num, check_num());
	
	num = 20;
	printf("check(%d) = %d\n", num, check_num());
	
	num = 15;
	printf("check(%d) = %d\n", num, check_num());
	
	return 0;

}	
	
