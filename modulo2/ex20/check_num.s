.section .data
	.global num
	
.section .text	
	.global check_num

check_num:
	#prologue
	pushl %ebp		# save previous stack frame pointer
	movl %esp, %ebp	# the stack frame pointer for sum function
	
	movl num, %eax
	
	cmpl $0, %eax
	
	jl negative
	jmp positive

positive:
	movl num, %eax
	movl $2, %ecx
	cdq
	idivl %ecx
	cmpl $0, %edx
	je positive_even
	movl $4, %eax
	jmp end
	
negative:
	movl num, %eax
	movl $2, %ecx
	cdq
	idivl %ecx
	cmpl $0, %edx
	je negative_even
	movl $2, %eax
	jmp end

positive_even:
	movl $3, %eax
	jmp end

negative_even:
	movl $1, %eax

end:
	#epilogue
	movl %ebp, %esp	# restore the previous stack pointer ("clear" the stack)
	popl %ebp		# restore the previous stack frame pointer
	ret
