.section .data
	.global A
	.global B
	
.section .text
	.global isMultiple
	
isMultiple:
	# prologue
	pushl %ebp		# save previous stack frame pointer
	movl %esp, %ebp	# the stack frame pointer for sum function
	
	cmpl $0, B
	je jmp_divisor_zero
	
	movl A, %eax
	movl B, %ecx
	movl $0, %edx
	
	divl %ecx
	cmpl $0, %edx
	je jmp_isMultiple
	movl $0, %eax
	
	jmp end

jmp_divisor_zero:
	movl $0, %eax
	jmp end

jmp_isMultiple:
	movl $1, %eax
	jmp end
	
end:
	# epilogue
	movl %ebp, %esp	# restore the previous stack pointer ("clear" the stack)
	popl %ebp		# restore the previous stack frame pointer
	ret
