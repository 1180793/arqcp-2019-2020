#include <stdio.h>
#include "isMultiple.h"

int A = 100;
int B = 10;

int main(void){
	
	printf("A = %d\n", A);
	printf("B = %d\n", B);
	int res = 0;
	res = isMultiple();
	if(res == 0){
		printf("%d is not multiple of %d\n\n", A, B);
	} else {
		printf("%d is multiple of %d\n\n", A, B);
	}
	
	A = 71;
	printf("A = %d\n", A);
	printf("B = %d\n", B);
	res = isMultiple();
	if(res == 0){
		printf("%d is not multiple of %d\n\n", A, B);
	} else {
		printf("%d is multiple of %d\n\n", A, B);
	}
	
	return 0;
}
