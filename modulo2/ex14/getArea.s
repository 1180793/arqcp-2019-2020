.section .data
	.global base
	base: .int 6
	.global height
	height: .int 8
	
.section .text
	.global getArea # int getArea(void)
	
getArea:
	# prologue
	pushl %ebp		# save previous stack frame pointer
	movl %esp, %ebp	# the stack frame pointer for sum function
	
	movl height, %ecx
	movl base, %eax

	mull %ecx

	movl $2, %ecx

	divl %ecx
	
	# epilogue
	movl %ebp, %esp	# restore the previous stack pointer ("clear" the stack)
	popl %ebp		# restore the previous stack frame pointer
	ret
