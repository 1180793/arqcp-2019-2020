#include <stdio.h>
#include "getArea.h"

extern int base;
extern int height;

int main(void){
	
	int result = 0;
	
	result = getArea();
	printf("(%d * %d) / 2 = %d\n",base, height, result);
	
	return 0;
}
