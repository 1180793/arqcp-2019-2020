#include <stdio.h>
#include "getArea.h"

int base = 6;
int height = 8;

int main(void){
	
	int result = 0;
	result = getArea();
	printf("(%d * %d) / 2 = %d\n", base, height, result);
	
	return 0;
}
