.section .data
	.global s1
	s1: .int 3987
	.global s2
	s2: .int 1903

.section .text
	.global crossSumBytes

crossSumBytes:
	#prologue
	pushl %ebp		# save previous stack frame pointer
	movl %esp, %ebp	# the stack frame pointer for sum function
	
	movw s1, %ax
	movw s2, %cx
	addb %cl, %ah
	addb %ch , %al
	
	# epilogue
	movl %ebp, %esp	# restore the previous stack pointer ("clear" the stack)
	popl %ebp		# restore the previous stack frame pointer
	ret
	