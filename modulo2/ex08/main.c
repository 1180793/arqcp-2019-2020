#include <stdio.h>
#include "crossSumBytes.h"

int main(void){
	
	int result = 0;
	
	printf("Short1: 0x%04X\n", 3987);
	printf("Short2: 0x%04X\n", 1903);
	
	result = crossSumBytes();
	printf("Result(%d): 0x%08X\n", result, result);
	
	return 0;
}
