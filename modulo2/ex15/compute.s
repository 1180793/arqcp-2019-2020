.section .data
	.global A
	.global B
	.global C
	.global D

.section .text
	.global compute # int compute(void)

compute:
	# prologue
	pushl %ebp		# save previous stack frame pointer
	movl %esp, %ebp	# the stack frame pointer for sum function


	movl D, %eax
	cmpl $0, D
	je jmp_divisor_zero

	movl A, %ecx
	movl B, %eax

	imul %ecx, %eax
	movl C, %ecx
	addl %ecx, %eax
	movl D, %ecx
	cdq
	idivl %ecx
	jmp end
	

jmp_divisor_zero:
	movl $0, %eax
	jmp end


end:
	# epilogue
	movl %ebp, %esp	# restore the previous stack pointer ("clear" the stack)
	popl %ebp		# restore the previous stack frame pointer
	ret
	