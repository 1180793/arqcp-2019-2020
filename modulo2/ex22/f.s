.section .data
	.global i				# 32-bit unsigned integer
	.global j				# 32-bit unsigned integer

.section .text
	.global f				# int f(void)
	.global f2				# int f2(void)
	.global f3				# int f3(void)
	.global f4				# int f4(void)

f:
	# prologue
	pushl %ebp      	# save previous stack frame pointer
	movl %esp, %ebp  	# the stack frame pointer for sum function
	
	movl $0, %eax
	movl $0, %ecx
	
	movl i, %eax
	movl j, %ecx
	
	cmpl %eax, %ecx
	je jmp_f_equal		
	
	addl %ecx, %eax		#else h=i+j
	decl %eax			#and h=h-1
	jmp end
	
jmp_f_equal:
	subl %ecx, %eax	#h=i-j
	incl %eax		#and h=h+1
	jmp end	
	
# ---------------------------------------------------
	
f2:
	# prologue
	pushl %ebp      	# save previous stack frame pointer
	movl %esp, %ebp  	# the stack frame pointer for sum function
	
	movl $0, %eax
	movl $0, %ecx

	movl i, %eax
	movl j, %ecx
	
	cmpl %ecx, %eax		#eax - ecx
	jg jmp_f2_greater
	
	incl %ecx			#else j = j + 1
	imull %ecx			#and h = i * j
	jmp end
	
jmp_f2_greater:
	decl %eax 	#i = i - 1
	imull %ecx 	#h = i * j
	jmp end
	
# ---------------------------------------------------

f3:
	# prologue
	pushl %ebp      	# save previous stack frame pointer
	movl %esp, %ebp  	# the stack frame pointer for sum function
	
	movl $0, %eax		
	movl $0, %ecx		
	movl $0, %edx		

	movl i, %eax		
	movl j, %ecx		
	
	cmpl %ecx, %eax			#eax - ecx
	jge jmp_f3_greater_equal
	
	movl %ecx, %edx
	addl %eax, %ecx		#h = i + j
	addl %edx, %eax		#g = i + j
	addl $2, %eax		#g = i + j + 2
	movl $0, %edx
	cdq
	idivl %ecx
	jmp end
	
jmp_f3_greater_equal:
	imull %eax, %ecx	#h = i * j
	addl $1, %eax		#g = i + 1
	movl $0, %edx
	cdq
	idivl %ecx
	jmp end
	
# ---------------------------------------------------

f4:
	# prologue
	pushl %ebp      	# save previous stack frame pointer
	movl %esp, %ebp  	# the stack frame pointer for sum function
	
	movl $0, %eax
	movl $0, %ecx
	movl $0, %edx

	movl i, %eax
	movl j, %ecx
	
	addl i, %ecx
	
	cmp $10, %ecx		# ecx - 10
	jng jmp_f4_not_greater
	
	movl $0, %eax		
	movl j, %eax		
	imull j				#h = j * j
	movl $0, %ecx		#clears ecx
	movl $3, %ecx		#moves 3 to ecx
	idivl %ecx			#h = (j * j) / 3
	
	jmp end
	
jmp_f4_not_greater:
	imull i			#h = i * i
	movl $0,%ecx
	movl $4, %ecx
	imull %ecx		#h = i * i * 4
	jmp end
	
# ---------------------------------------------------
	
end:
	# epilogue
	movl %ebp, %esp  	# restore the previous stack pointer ("clear" the stack)
	popl %ebp     		# restore the previous stack frame pointer
	ret
