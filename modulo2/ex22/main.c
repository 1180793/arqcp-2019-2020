#include <stdio.h>
#include "f.h"

int i = 0, j = 0;

int main(void) {

	i = 2;
	j = 2;
	printf("f(%d, %d) = %i\n", i, j, f());
	
	i = 2;
	j = 3;
	printf("f(%d, %d) = %i\n", i, j, f());
	
	i = 3;
	j = 2;
	printf("f2(%d, %d) = %i\n", i, j, f2());
	
	i = 2;
	j = 3;
	printf("f2(%d, %d) = %i\n", i, j, f2());
	
	i = 1;
	j = 32;
	printf("f3(%d, %d) = %i\n", i, j, f3());
	
	i = 8;
	j = 2;
	printf("f3(%d, %d) = %i\n", i, j, f3());
	
	i = 2;
	j = 3;
	printf("f3(%d, %d) = %i\n", i, j, f3());
	
	i = 2;
	j = 2;
	printf("f4(%d, %d) = %i\n", i, j, f4());
	
	i = 5;
	j = 6;
	printf("f4(%d, %d) = %i\n", i, j, f4());
	
	return 0;
}
