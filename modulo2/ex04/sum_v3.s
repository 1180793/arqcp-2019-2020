.section .data
	.global op1
	.global op2
	.global op3
	op3: .int 4
	.global op4
	op4: .int 5
	
.section .text
	.global sum_v3	# int sum_v3(void)
	
sum_v3:
	# prologue
	pushl %ebp		# save previous stack frame pointer
	movl %esp, %ebp	# the stack frame pointer for sum function
	
	movl op4, %eax
	addl op3, %eax
	sub op2, %eax
	addl op1, %eax
	
	# epilogue
	movl %ebp, %esp  #  restore the previous stack pointer ("clear" the stack)
	popl %ebp     #  restore the previous stack frame pointer
	ret
