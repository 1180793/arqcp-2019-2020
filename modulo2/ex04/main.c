#include <stdio.h>
#include "sum_v3.h"

int op1 = 0, op2 = 0;

int main(void) {
	
	printf("Valor op1: ");
	scanf("%d",&op1);
	
	printf("Valor op2: ");
	scanf("%d",&op2);
	
	extern int op3;
	printf("Valor op3: %d\n", op3);
	
	extern int op4;
	printf("Valor op4: %d\n", op4);
	
	int res = sum_v3();
	printf("%d + %d - %d + %d = %d:0x%x\n", op4, op3, op2, op1, res, res);
	
	return 0;
}
