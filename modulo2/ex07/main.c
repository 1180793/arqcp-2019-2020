#include <stdio.h>
#include "crossSumBytes.h"

short int s1 = 0;
short int s2 = 0;

int main(void){
	
	int result = 0;
	s1 = 3987;
	s2 = 1903;
	
	printf("Short1: 0x%04X\n", s1);
	printf("Short2: 0x%04X\n", s2);
	
	result = crossSumBytes();
	printf("Result(%d): 0x%08X\n", result, result);
	
	return 0;
}
