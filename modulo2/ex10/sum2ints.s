.section .data	
	.global op1
	.global op2

.section .text
	.global sum2ints;
	
sum2ints:
	# prologue
	pushl %ebp		# save previous stack frame pointer
	movl %esp, %ebp	# the stack frame pointer for sum function
	pushl %ebx
    pushl %edi
	pushl %esi
		
	movl $0,%edx
	movl op1,%eax
	cdq
	movl %eax,%ecx 	# HIGHEST PART
	movl %edx,%ebx 	# LOWEST PART
	
	movl $0,%edx
	movl op2, %eax
	cdq
	addl %eax, %ecx
	adcl %edx, %ebx
	
	movl %ecx, %eax
	movl %ebx, %edx
	
	# epilogue
    popl %esi
	popl %edi
	popl %ebx
	movl %ebp, %esp	# restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
