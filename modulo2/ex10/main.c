#include <stdio.h>
#include "sum2ints.h"

int op1 = 0x00012345;
int op2 = 0x0126789A;

int main(void) {
	
	printf("OP1: %d : 0x%X\n", op1, op1);
	printf("OP2: %d : 0x%X\n", op2, op2);
	
	long long int res = sum2ints();
	printf("Result = %lld : 0x%llX\n", res, res);
	
	return 0;
}
