#include <stdio.h>
#include "new_salary.h"

int code = 0, currentSalary = 0;

int main(void) {
	
	int newSalary = 0;
	code = 10;
	currentSalary = 1500;
	
	newSalary = new_salary();
	printf("CODE: %d | %d€ -> %d€\n", code, currentSalary, newSalary);
	
	code = 11;
	currentSalary = 1400;
	
	newSalary = new_salary();
	printf("CODE: %d | %d€ -> %d€\n", code, currentSalary, newSalary);
	
	code = 12;
	currentSalary = 1200;
	
	newSalary = new_salary();
	printf("CODE: %d | %d€ -> %d€\n", code, currentSalary, newSalary);
	
	code = 15;
	currentSalary = 1000;
	
	newSalary = new_salary();
	printf("CODE: %d | %d€ -> %d€\n", code, currentSalary, newSalary);
	
	return 0;
}
