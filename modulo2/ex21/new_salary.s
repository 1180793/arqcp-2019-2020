.section .data
	.global code
	.global currentSalary

.section .text
	.global new_salary # int new_salary(void)

new_salary:
	# prologue
	pushl %ebp		# save previous stack frame pointer
	movl %esp, %ebp	# the stack frame pointer for sum function

	movl currentSalary, %eax
	movl code, %ecx
	
	cmpl $10, %ecx
	je code_10
	cmpl $11, %ecx
	je code_11
	cmpl $12, %ecx
	je code_12
	
	addl $100, %eax
	jmp end
	
code_10:
	addl $300, %eax
	jmp end
	
code_11:
	addl $250, %eax
	jmp end
	
code_12:
	addl $150, %eax
	jmp end
	
end:
	# epilogue
	movl %ebp, %esp	# restore the previous stack pointer ("clear" the stack)
	popl %ebp		# restore the previous stack frame pointer
	ret
	