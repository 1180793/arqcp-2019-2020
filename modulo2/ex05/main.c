#include <stdio.h>
#include "swapBytes.h"

short int s = 12;

int main(void){
	
	printf("Short Int: %d\n", s);
	printf("Initial Value: 0x%04X\n", s);
	
	s = swapBytes();
	printf("Swapped value: 0x%04X\n", s);
	
	return 0;
}
