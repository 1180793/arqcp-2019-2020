.section .data
	.global s

.section .text
	.global swapBytes

swapBytes:
	#prologue
	pushl %ebp       # save previous stack frame pointer
	movl %esp, %ebp  # the stack frame pointer for sum function
	pushl %ebx
	
	movl $0, %eax
	movl $0, %ebx
	movw s, %bx 
	movb %bl, %ah    # bl - bits menos significativos de %bx
	movb %bh, %al    # bh - bits mais significativos de %bx
	
	# epilogue
	popl %ebx
	movl %ebp, %esp  # restore the previous stack pointer ("clear" the stack)
	popl %ebp        # restore the previous stack frame pointer
	ret
