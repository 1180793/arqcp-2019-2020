.section .data
	.global op1
	.global op2
	.global res
	
.section .text
	.global sum 			# int sum(void)
	.global subtraction 	# int subtraction(void)
	.global multiplication 	# int multiplication(void)
	.global division 		# int division(void)
	.global modulus 		# int modulus(void)
	.global powerOfTwo 		# int powerOfTwo(void)
	.global powerOfThree 	# int powerOfThree(void)
	
	
sum:
	# prologue
	pushl %ebp      	# save previous stack frame pointer
	movl %esp, %ebp 	# the stack frame pointer for sum function
	
	movl op1, %eax
	movl op2, %ecx
	
	addl %ecx, %eax
	movl %eax, res
	jmp end
	
subtraction:
	# prologue
	pushl %ebp      # save previous stack frame pointer
	movl %esp, %ebp  # the stack frame pointer for sum function
	
	movl op1, %eax
	movl op2, %ecx

	subl %ecx, %eax
	movl %eax, res
	jmp end
	
multiplication:
	# prologue
	pushl %ebp      # save previous stack frame pointer
	movl %esp, %ebp  # the stack frame pointer for sum function
	
	movl op1, %eax
	movl op2, %ecx

	imull %ecx, %eax
	movl %eax, res
	jmp end
	
division:
	# prologue
	pushl %ebp      # save previous stack frame pointer
	movl %esp, %ebp  # the stack frame pointer for sum function
	
	cmpl $0, op2
	je jmp_divisor_zero
	
	movl op1, %eax
	movl $0, %edx
	cdq
	idivl op2
	movl %eax, res
	jmp end
	
modulus:
	# prologue
	pushl %ebp      # save previous stack frame pointer
	movl %esp, %ebp  # the stack frame pointer for sum function
	
	cmpl $0, op2
	je jmp_divisor_zero
	
	movl op1, %eax
	movl op2, %ecx
	movl $0, %edx
	cdq
	idivl %ecx
	movl %edx, %eax
	movl %eax, res
	jmp end
	
powerOfTwo:
	# prologue
	pushl %ebp      # save previous stack frame pointer
	movl %esp, %ebp  # the stack frame pointer for sum function
	
	movl op1, %eax
	mull %eax
	movl %eax, res
	jmp end
	
powerOfThree:
	# prologue
	pushl %ebp      # save previous stack frame pointer
	movl %esp, %ebp  # the stack frame pointer for sum function
	
	movl op1, %eax
	movl op1, %ecx
	imull %eax, %eax
	imul %ecx, %eax
	movl %eax, res
	jmp end

jmp_divisor_zero:
	movl $0, %eax
	movl %eax, res
	jmp end
	
end:
	# epilogue
	movl %ebp, %esp  #  restore the previous stack pointer ("clear" the stack)
	popl %ebp     #  restore the previous stack frame pointer
	ret
	