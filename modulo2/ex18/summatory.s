.section .data
    .global A
	.global B
	.global i
	A: .int 5
	B: .int 10
	
.section .text
	.global summatory
	
summatory:
    # prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	pushl %esi
	pushl %edi
	pushl %ebx
	
    movl $1 , %esi
	movl B , %ebx
	movl $0 , %ecx
	
loop:
    movl %esi , %eax
    imull %eax
	movl %eax , %edi
	
	movl A , %eax
	imull %eax
	
	imull %edi
	
	idivl %ebx
	
	addl %eax , %ecx
	
	incl %esi
	cmpl i , %esi
	jle loop
	
	movl %ecx , %eax

    popl %ebx
    popl %edi
	popl %esi

    # epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
