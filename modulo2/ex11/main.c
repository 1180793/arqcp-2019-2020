#include <stdio.h>
#include "test_flags.h"

int op1 = 0;
int op2 = 0;

int main(void){
	
	int result = 0;
	
	op1 = 0x00000001;
	op2 = 0x00000002;
	printf("OP1 = %d : 0x%X\n", op1, op1);
	printf("OP2 = %d : 0x%X\n", op2, op2);
	result = test_flags();
	printf("Result(No Flag): %d\n\n",result);
	
	op1 = 0x80000000;
	op2 = 0x80000000;
	printf("OP1 = %d : 0x%X\n", op1, op1);
	printf("OP2 = %d : 0x%X\n", op2, op2);
	result = test_flags();
	printf("Result(Carry Flag): %d\n\n",result);
	
	op1 = 0x40000000;
	op2 = 0x40000000;
	printf("OP1 = %d : 0x%X\n", op1, op1);
	printf("OP2 = %d : 0x%X\n", op2, op2);
	result = test_flags();
	printf("Result(Overflow Flag): %d\n",result);
	
	return 0;
}
