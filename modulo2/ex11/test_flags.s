.section .data
	.global op1
	.global op2
	
.section .text
	.global test_flags # char test_flags(void)
	
test_flags:
	# prologue
	pushl %ebp		# save previous stack frame pointer
	movl %esp, %ebp	# the stack frame pointer for sum function
	
	movl $0, %ecx
	movl $0, %eax
	movl op1, %ecx
	movl op2, %eax
	
	addl %ecx, %eax
	
	jc jump_if_carry
	jo jump_if_overflow
	
	movl $0, %eax
	
	jmp end
	
jump_if_carry:
	movb $1, %al
	jmp end
	
jump_if_overflow:
	movb $1, %al
	jmp end
	
end:
	# epilogue
	movl %ebp, %esp	# restore the previous stack pointer ("clear" the stack)
	popl %ebp		# restore the previous stack frame pointer
	ret
