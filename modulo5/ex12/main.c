#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "find_matrix.h"
#include "new_matrix.h"


int main(){
	
	int lines = 4;
	int col = 5;
	int num = 10;
	time_t t;
	int ** matrix = new_matrix(lines,col);
	srand((unsigned) time(&t));
	
	int i,j;
	/**
	 * preencher a matriz
	 **/
	for(i=0;i<lines;i++)
	{
		for(j=0;j<col;j++)
		{
			matrix[i][j] = rand();
		}
	}
	
	int r=0;
	r=find_matrix(matrix,lines,col,num);
	
	if(r==0)
	{
		printf("\nResultado : %d\nO valor não existe em m\n",find_matrix(matrix,lines,col,num));
	}
	
	if(r==1)
	{
		printf("\nResultado : %d\nO valor existe em m\n",find_matrix(matrix,lines,col,num));
	}
	
	for (i = 0; i < lines; i++)
	{
		free(*(matrix+i));
	}
	
	free(matrix);
	return 0;
}
