#include <stdio.h>

union union_u1 {
	char vec[32];
	float a;
	int b;
} u;

struct struct_s1 {
	char vec[32];
	float a;
	int b;
} s;

int main() {

	printf("Union: %i\n", sizeof(u));
	printf("Struct: %i\n", sizeof(s));
	
	printf("\nNa union o tamanho é 32 porque corresponde ao valor máximo dos seus atributos.\n");
	printf("Na struct o tamanho é 40 porque é a soma de todos os seus atributos.\n\n");
	
	return 0;
}	


