.section .data
	.equ OFFSET, 4
	
.section .text
	.global approved_semester
	
approved_semester:
    # prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	subl $4 , %esp
	
	pushl %esi
	pushl %edi
	pushl %ebx
	
	movl $0, %eax
	
	movl 8(%ebp), %esi
	movl (%esi) , %ecx
	movl %ecx , -4(%ebp)
	
	movl $-1 , %edi
	
	movl OFFSET(%esi), %ecx
	
loop1:
    incl %edi
    cmpl -4(%ebp) , %edi
    je end

    movsxb (%ecx, %edi, 1), %esi
	movl $0 ,%edx
	movl $0 ,%ebx
loop2:
	cmpl $8, %edx
	je passed
	
	shrl %esi
	jc one
	incl %edx
	jmp loop2
	
one:
    incl %ebx
    incl %edx
    jmp loop2

passed:
    cmpl $5	, %ebx
	jl loop1
	
	incl %eax
	jmp loop1
	
end:
    popl %ebx
    popl %edi
    popl %esi
    # epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
	
	
	
