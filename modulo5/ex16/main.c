#include <stdio.h>
#include <stdlib.h>
#include "group.h"
#include "asm.h"



int main()
{
		
		group *g  = (group*)malloc(1 * sizeof(group));
		
		g->n_students= 4;	
		g->individual_grades = (unsigned char*) malloc(g->n_students);
		
		*(g->individual_grades + 0) = 0b11111111;
		*(g->individual_grades + 1) = 0b01110110;
		*(g->individual_grades + 2) = 0b01110110;
		*(g->individual_grades + 3) = 0b00000000;
		
		printf("\nForam aprovados %d alunos \n", approved_semester(g));
		
		free(g->individual_grades);
		free(g);
		
	return 0;
}
