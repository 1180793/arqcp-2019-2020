#include <stdio.h>
#include <string.h>

int main (void)
{
	/*
	union union_u1				//O apontador não muda de posição, logo os valores das 3 últimas impressões estão errados, devido a uma variável poder ser de vários tipos.
	{
		char vec[32];
		float a;
		int b;
	}u;
	
	union union_u1 * ptr = &u;
	*/
	struct struct_s1			//Os valores estão corretos, porque uma structure pode agregar várias variáveis no mesmo nome
	{
		char vec[32];
		float a;
		int b;
	}s;
	
	struct struct_s1 * ptr = &s;
	
	
	strcpy(ptr->vec, "arquitectura de computadores" );
	printf( "[1]=%s\n", ptr->vec);
	ptr->a = 123.5;
	printf( "[2]=%f\n", ptr->a);
	ptr->b = 2;
	printf( "[3]=%d\n", ptr->b);
	
	printf( "[1]=%s\n", ptr->vec);
	printf( "[2]=%f\n", ptr->a);
	printf( "[3]=%d\n", ptr->b);
	return 0;
}
