#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

void push(Stack *stack, int value) {
	stack->size = stack->size + 1;
	stack->stored = (int *) realloc(stack->stored, stack->size * sizeof(int));
	
	if (stack->stored == NULL){
		printf ("Error reserving memory .\n");
		stack->stored[stack->size-1] = -1;
	}
	
	stack->stored[stack->size-1] = value;
}