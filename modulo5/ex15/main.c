#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
#include "push.h"
#include "pop.h"
#include "size.h"

int main(void){
	
	Stack st;
	Stack *stack= &st;
	
	stack->stored = (int*) malloc(0);
	stack->size = 0;
	
	
	int value = 32;
	push(stack, value);
	
	printf("\nTeste Push\n");
	printf("Valor Esperado: 32\n");
	printf("Valor Obtido: %d\n", stack->stored[stack->size-1]);
	
	push(stack, 33);
	push(stack, 34);
	push(stack, 35);
	push(stack, 36);
	
	printf("\nTeste Pop\n");
	printf("Valor Esperado: 36\n");
	printf("Valor Obtido: %d\n", pop(stack));
	
	printf("\nTeste Size\n");
	printf("Valor Esperado: 4\n");
	printf("Valor Obtido: %d\n", size(stack));
	
	free (stack->stored);

	return 0;
}
