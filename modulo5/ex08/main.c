#include <stdio.h>
#include "s2.h"
#include "asm.h"

int main(void)
{

    s2 estrutura;
    s2  *estrutura_ptr = &estrutura;
    short w[3];
    w[0] = 1;
    w[1] = 2;
    w[2] = 3;

    int j = 4;
    char c[] = "abc";

    fill_s2(estrutura_ptr, w, j, c);
    
    printf("deve ser 1=%d \ndeve ser 2=%d \ndeve ser 3=%d",estrutura.w[0],estrutura.w[1],estrutura.w[2]);
    printf("\ndeve ser 4=%d",estrutura.j);
    printf("\ndeve ser abc = ");
	int i;
	for (i = 0; i < 3; i++)
	{
		printf("%c",estrutura.c[i]);
	}
	printf("\n");
    return 0;
}
