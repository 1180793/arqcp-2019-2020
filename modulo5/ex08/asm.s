.section .data

.equ W_OFFSET,0
.equ J_OFFSET,8
.equ C_OFFSET,12


.section .text
	.global fill_s2
	fill_s2:
	
	#prologue
	pushl %ebp  #save previous stack frame pointer
	movl %esp, %ebp #the stack frame pointer for sum function

    pushl %esi

    movl 8(%ebp),%esi   # %esi estrutura_ptr
    movl $0,%ecx        # %ecx contador
    movl 12(%ebp),%eax
    
ciclo_w:
    cmp $3,%ecx
    je ciclo_j

    movw (%eax),%dx
    movw %dx,W_OFFSET(%esi,%ecx,2)
    
    
    addl $2,%eax
    incl %ecx

    jmp ciclo_w

ciclo_j:

    movl 16(%ebp),%eax
    movl %esi,%edx
    addl $J_OFFSET,%edx
    movl %eax, (%edx)    

    movl $0,%ecx

	movl 20(%ebp),%eax
	movl $0,%edx

ciclo_c:
    cmp $3,%ecx
    je fim
	
	movb (%eax),%dl
    movb %dl,C_OFFSET(%esi,%ecx,1)

    incl %eax
    incl %ecx
    jmp ciclo_c

fim: 
	
    popl %esi
    
    #epilogue
	movl %ebp, %esp #restore the previous stack pointer ("clear" the stack)
	popl %ebp #restore the previous stack frame pointer
	ret 
