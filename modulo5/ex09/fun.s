.section .data
	.equ A_DATA_SIZE, 8		# struct structA = {structA a, structA *b, int x, char c, int y, char[3] e, short z}
	.equ A_X_OFFSET, 0		# short x	- 2 bytes
	.equ A_Y_OFFSET, 4		# int y		- 4 bytes	
	
	.equ B_DATA_SIZE, 30	# struct structB = {short x, int y}
	.equ B_A_OFFSET, 0		# structA a - 8 bytes(A_DATA_SIZE)
	.equ B_B_OFFSET, 8		# structA* b- 4 bytes
	.equ B_X_OFFSET, 12		# int x 	- 4 bytes
	.equ B_C_OFFSET, 16		# char c 	- 1 byte
	.equ B_Y_OFFSET, 20		# int y		- 4 bytes 
	.equ B_E_OFFSET, 24		# char[3] e - 3 bytes 
	.equ B_Z_OFFSET, 28		# short z	- 2 bytes
	
.section .text
	.global fun1 # short fun1(structB *s) { return s->a.x; }
	.global fun2 # short fun2(structB *s) { return s->z; }
	.global fun3 # short* fun3(structB *s) { return &s->z; }
	.global fun4 # short fun4(structB *s) { return s->b->x; }
	
fun1:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %esi
	
	movl 8(%ebp), %esi			# structB -> %esi
	movl B_A_OFFSET(%esi), %eax	# como x é o primeiro elemento de structA
	
	popl %esi
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret

fun2:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %esi
	
	movl 8(%ebp), %esi			# structB -> %esi
	movl B_Z_OFFSET(%esi), %eax
	
	popl %esi
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
	
fun3:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %esi
	
	movl 8(%ebp), %esi			# structB -> %esi
	leal B_Z_OFFSET(%esi), %eax
	
	popl %esi
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
	
fun4:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %esi
	
	movl 8(%ebp), %esi			# structB -> %esi
	movl B_B_OFFSET(%esi), %edx	
	movl (%edx), %eax
	
	popl %esi
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
