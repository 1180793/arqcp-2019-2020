#include <stdio.h>
#include <string.h>
#include "structs.h"
#include "fun.h"

int main(void){
	structA aux;
	aux.x = 80;
	aux.y = 81;
	structB estrutura;
	structB *s= &estrutura;
	
	s->a.x = 2;
	s->a.y = 3;
	s->b = &aux;
	s->x = 12;
	s->c = 13;
	s->y = 14;
	strcpy(estrutura.e,"abc");
	s->z = 50;
	
	printf("\nFuncao 1\n");
	printf("Valor Esperado = %d\n", s->a.x);
	printf("Valor Obtido = %d\n", fun1(s));
	
	printf("\nFuncao 2\n");
	printf("Valor Esperado = %d\n", s->z);
	printf("Valor Obtido = %d\n", fun2(s));
	
	printf("\nFuncao 3\n");
	printf("Valor Esperado = %p\n", &s->z);
	printf("Valor Obtido = %p\n", fun3(s));
	
	printf("\nFuncao 4\n");
	printf("Valor Esperado = %d\n", s->b->x);
	printf("Valor Obtido = %d\n", fun4(s));
	

	return 0;	
}

// typedef struct {
//	short x;
//	int y;
//} structA;

//typedef struct {
//	structA a;
//	structA *b;
//	int x;
//	char c;
//	int y;
//	char e[3];
//	short z;
//} structB;
