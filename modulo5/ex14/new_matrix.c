#include <stdio.h>
#include <stdlib.h>

int **new_matrix(int lines, int columns){
	
	int ** mat;
	int i;
	mat = (int**)calloc(lines, sizeof(int*));
	for(i=0;i<lines;i++)
	{
		mat[i] = (int*)calloc(columns, sizeof(int));	
	}
		
	return mat;
}

