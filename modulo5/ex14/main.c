#include <stdio.h>
#include <stdlib.h>
#include "new_matrix.h"
#include "add_matrixes.h"

int main(){
	int y=3,k=4;
	int i,j;
	int ** result;
	
	int** a = new_matrix(y,k);
	int** b = new_matrix(y,k);
	
	for(i=0;i<y;i++)
	{
		for(j=0;j<k;j++)
		{
			a[i][j] = i+j;
			b[i][j] = i+j;
		}
		
	}
	result = add_matrixes(a,b,y,k);
	
	printf("Matriz A\n");
	for(i=0;i<y;i++)
	{
		for(j=0;j<k;j++)
		{
			printf("%d ",a[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	
	printf("Matriz B\n");
	for(i=0;i<y;i++)
	{
		for(j=0;j<k;j++)
		{
			printf("%d ",b[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	
	printf("Soma matrizes\n");
	for(i=0;i<y;i++)
	{
		for(j=0;j<k;j++)
		{
			printf("%d ",result[i][j]);
		}
		printf("\n");
	}
	
	for (i = 0; i < y; i++)
	{
		free(*(a+i));
		free(*(b+i));
		free(*(result+i));
	}
	free(a);
	free(b);
	free(result);
	
	return 0;
}
