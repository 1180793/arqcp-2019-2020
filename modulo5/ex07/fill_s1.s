.section .data
	.equ DATA_SIZE, 244
	
	.equ I_OFFSET, 0 		# int i			- 4 bytes
	.equ C_OFFSET, 4		# char c 		- 1 byte
	.equ J_OFFSET, 8		# int j			- 4 bytes
	.equ D_OFFSET, 12		# char d 		- 1 byte
	
.section .text
	.global fill_s1 # void fill_s1(s1 *s, int vi, char vc, int vj, char vd)
	
fill_s1:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %esi
	
	movl 8(%ebp), %esi			# s1 -> %esi
	
	movl 12(%ebp), %edx			# vi -> %edx
	movl %edx, I_OFFSET(%esi)
	
	movb 16(%ebp), %al			# vc -> %al
	movb %al, C_OFFSET(%esi)
	
	movl 20(%ebp), %edx			# vj -> %edx
	movl %edx, J_OFFSET(%esi)
	
	movb 24(%ebp), %al			# vd -> %al
	movb %al, D_OFFSET(%esi)
	
	
end:
	popl %esi
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
