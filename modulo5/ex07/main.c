#include <stdio.h>
#include "s1.h"
#include "fill_s1.h"

int main() {
	
	s1 st;
	
	s1* s = &st;
	fill_s1(s, 13, 'a', 10, 'b'); 
	printf("Vi = %d\n", s->i);
    printf("C1 = %c\n", s->c);
    printf("Vj = %d\n", s->j);
    printf("C2 = %c\n", s->d);
	
	return 0;
}	