.section .data
	.equ STRUCTA_TOTAL, 28
	.equ STRUCTA_UB_OFFSET, 20
	
.section .text
	.global return_unionB_b # char return_unionB_b(structA **matrix, int i, int j) { return matrix[i][j].ub.b; }
	
return_unionB_b:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %ebx
	pushl %edi
	pushl %esi
	
	movl 8(%ebp), %edi			# matrix - edi
	movl 12(%ebp), %ecx			# i - ecx
	
	movl (%edi, %ecx, 4), %esi	# guarda em esi a posiçao do inicio da linha i
	
	movl 16(%ebp), %ecx			# j - ecx
	imul $STRUCTA_TOTAL, %ecx	# j * total bytes por elemento
	addl %ecx, %esi				# adiciona o total de bytes até chegar ao elemento j
	
	addl $STRUCTA_UB_OFFSET, %esi	# adiciona o offset para chegar ao byte do atributo
	movl (%esi), %eax
	
	popl %esi
	popl %edi
	popl %ebx
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
	