#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "structs.h"
#include "new_matrix.h"
#include "return_unionB_b.h"

int main(void) {

	structA estrutura;
	
	estrutura.a[0] = 1;
	estrutura.a[1] = 2;
	estrutura.a[2] = 3;
	estrutura.b = 'b';
	estrutura.c = 180;
	estrutura.d = 10;
	estrutura.ub.b = 'A';
	estrutura.e = 'e';
	
	structA** matriz = new_matrix(4,3);
	int i, j;
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 3; j++) {
			matriz[i][j] = estrutura;
		}
	}
	
	printf("\nM[0][0]\nValor Esperado: = %c\n", matriz[0][0].ub.b);
	printf("Valor Obtido: = %c\n", return_unionB_b(matriz, 0, 0));
	
	estrutura.ub.b = 'G';
	matriz[1][1] = estrutura;
	printf("\nM[1][1]\nValor Esperado: = %c\n", matriz[1][1].ub.b);
	printf("Valor Obtido: = %c\n", return_unionB_b(matriz, 1, 1));
	
	
	estrutura.ub.b = 'M';
	matriz[2][1] = estrutura;
	printf("\nM[2][1]\nValor Esperado: = %c\n", matriz[2][1].ub.b);
	printf("Valor Obtido: = %c\n", return_unionB_b(matriz, 2, 1));
	
	
	estrutura.ub.b = 'S';
	matriz[3][2] = estrutura;
	printf("\nM[3][2]\nValor Esperado: = %c\n", matriz[3][2].ub.b);
	printf("Valor Obtido: = %c\n\n", return_unionB_b(matriz, 3, 2));
	
	for (i = 0; i < 4 ; i++) {
		free (*(matriz + i));
	}
	free(matriz);
	
	return 0;		
}