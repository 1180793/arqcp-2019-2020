#include <stdio.h>
#include <stdlib.h>
#include "structs.h"

structA **new_matrix(int lines, int columns) {
	
	structA **m = (structA**) calloc(lines, sizeof(structA*)); //coluna de apontadores para cada linha
	if (m == NULL){
		printf ("Error reserving memory .\n");
		return NULL;
	}
	
	int i;
	for (i = 0; i < lines ; i++) {
		*(m + i) = (structA *) calloc (columns , sizeof(structA)); // Note : *(m+i) same as m[i]
		if(*(m + i) == NULL ){
			printf ("Error reserving memory .\n"); 
			return NULL;
		}
	}
	
	return m;
}