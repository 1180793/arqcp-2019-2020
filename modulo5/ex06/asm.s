.section .data

.equ GRADES_OFFSET,4


.section .text
	.global locate_greater
	locate_greater:
	
	#prologue
	pushl %ebp  #save previous stack frame pointer
	movl %esp, %ebp #the stack frame pointer for sum function
	pushl %esi
	pushl %ebx

	movl 8(%ebp),%esi		# esi struct
	movl 12(%ebp),%ebx		# ebx minimo
	movl 16(%ebp),%edx		# edx new_array
	movl $0,%ecx			# ecx contador
	movl $0,%eax
ciclo:
	cmp $10,%ecx		
	je fim
	
	
	cmp %ebx,GRADES_OFFSET(%esi,%ecx,4)
	jg incrementar

	jmp fim_ciclo

incrementar:
	
	pushl %eax
	movl GRADES_OFFSET(%esi,%ecx,4),%eax
	movl %eax,(%edx)
	addl $4,%edx
	popl %eax
	incl %eax

fim_ciclo:	

	incl %ecx
	jmp ciclo
fim: 
	popl %ebx
	popl %esi

	#epilogue
	movl %ebp, %esp #restore the previous stack pointer ("clear" the stack)
	popl %ebp #restore the previous stack frame pointer
	ret 
