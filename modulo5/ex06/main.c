#include <stdio.h>
#include "student.h"
#include "asm.h"

int main(void){
	
	Student estudante;
	
	
	estudante.grades[0]=1;
	estudante.grades[1]=12;
	estudante.grades[2]=13;
	estudante.grades[3]=11;
	estudante.grades[4]=20;
	estudante.grades[5]=17;
	estudante.grades[6]=12;
	estudante.grades[7]=5;
	estudante.grades[8]=9;
	estudante.grades[9]=10;

	Student *estudanteptr=&estudante; 
	
	int greater_grades[10];
	int *greater_gradesptr=greater_grades;
	int minimum=10;
	int n_changes;
	n_changes=locate_greater(estudanteptr,minimum,greater_gradesptr);
	

	printf("\nNumber of grades added to greater_grades - %d \n",n_changes);
	
	int i;
	printf("Grades greater than minimum : "); 
	for(i=0; i<n_changes;i++)
	{
		printf("%d,",greater_grades[i]);	
	}
	printf("\n"); 
	
	

	return 0;
	
}
