#include <stdio.h>
#include <stdlib.h>
#include "new_matrix.h"
#include "count_odd_matrix.h"

int main(void){
	int linhas = 3;
	int colunas = 4;
	
	int** matriz = new_matrix(linhas, colunas);
	
	*(*(matriz + 2) + 3) = 1;
	*(*(matriz + 2) + 2) = 1;
	*(*(matriz + 1) + 2) = 4;
	*(*(matriz + 2) + 0) = 1;
	*(*(matriz) + 1) = 1;
	**matriz = 3;
	
	printf("Endereço: %p \n", matriz);
	
	int i, j;
	for (i = 0; i < linhas; i++) {
		printf("|");
		for (j = 0; j < colunas; j++) {
			*(*(matriz + i) + j) = 1;
			printf("%d|", *(*(matriz + i) + j));
		}
		printf("\n");
	}
	
	printf("Existem %d números impares na Matriz\n", count_odd_matrix(matriz, linhas, colunas));
	
	for (i = 0; i < linhas ; i++) {
		free (*(matriz + i));
	}
	free(matriz);

	return 0;
}
