.section .data
	
.section .text
	.global count_odd_matrix # int count_odd_matrix(int** m, int y, int k)	(M[YxK])
	
count_odd_matrix:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	subl $4, %esp
	pushl %ebx
	pushl %edi
	pushl %esi
	
	movl 12(%ebp), %edx
	shll $2, %edx
	movl %edx, -4(%ebp)	# guarda o nr de bytes numa linha
	movl 8(%ebp), %esi
	movl $0, %ecx 	# contador linha
	movl $0, %eax	# contador impares
	movl $0, %edx	# contador elemento coluna
	
matrix_line_loop:
	cmpl 12(%ebp), %ecx
	jge end
	movl (%esi, %ecx, 4), %ebx	# Linha para ebx
	incl %ecx
	movl $0, %edx
	
	matrix_column_loop:
		cmpl 16(%ebp), %edx
		jge matrix_line_loop
		movl (%ebx, %edx, 4), %edi	# Elemento para edi
		incl %edx
		shr %edi
		jc jmp_impar
	jmp matrix_column_loop
	
jmp_impar:
	incl %eax
	jmp matrix_column_loop
	
end:
	popl %esi
	popl %edi
	popl %ebx
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
