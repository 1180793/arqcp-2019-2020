#include <stdio.h>
#include <string.h>
#include "student.h"
#include "update_address.h"


int main()
{
	Student st;
	
	Student *s = &st;
	strcpy(st.address, "4490 Póvoa de Varzim");
	printf("Morada antiga \n%s\n", st.address);
	char new_address[] = "4480 Vila do Conde";
	update_address(s, new_address); 
	printf("Morada actualizada \n%s\n", st.address);
	
	
	return 0;
}
