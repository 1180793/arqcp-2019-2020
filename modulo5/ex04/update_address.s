.section .data
		.equ OFFSET_ADDRESS, 124
.section .text
		.global update_address # void update_address(Student *s, char *new_address)
		
	
	
update_address: 


# prologue
	pushl %ebp # save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	
	movl 8(%ebp) , %ecx
	addl $OFFSET_ADDRESS , %ecx
	movl 12(%ebp), %edx

	
ciclo: 
	movb (%edx) ,%al
	movb %al,(%ecx)
	cmp $0,%al
	je end
	incl %ecx
	incl %edx
	jmp ciclo
	
	
end:

# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp # restore the previous stack frame pointer
	ret
