#include <stdio.h>
#include "student.h"
#include "fill_student.h"

int main() {
	
	Student st[5];
	
	Student *s = &st[0];
	fill_student(s, 19, 118, "Fernando", "Rua 0"); 
	printf("%d\n", s->age);
    printf("%d\n", s->number);
    printf("%s\n", s->name);
    printf("%s\n", s->address);
	
	s = &st[1];
	fill_student(s, 1, 1, "Fernando 1", "Rua 1"); 
	printf("%d\n", s->age);
    printf("%d\n", s->number);
    printf("%s\n", s->name);
    printf("%s\n", s->address);
	
	s = &st[2];
	fill_student(s, 2, 2, "Fernando 2", "Rua 2"); 
	printf("%d\n", s->age);
    printf("%d\n", s->number);
    printf("%s\n", s->name);
    printf("%s\n", s->address);
	
	s = &st[3];
	fill_student(s, 3, 3, "Fernando 3", "Rua 3"); 
	printf("%d\n", s->age);
    printf("%d\n", s->number);
    printf("%s\n", s->name);
    printf("%s\n", s->address);
	
	s = &st[4];
	fill_student(s, 4, 4, "Fernando 4", "Rua 4"); 
	printf("%d\n", s->age);
    printf("%d\n", s->number);
    printf("%s\n", s->name);
    printf("%s\n", s->address);
	
	return 0;
}	



