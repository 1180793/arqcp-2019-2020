#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *new_str(char str[80]) {
	int i = 0;
	char *ptr;
	int size = strlen(str) + 1;
	ptr = (char *) malloc(size * sizeof(char));
	
	for (i = 0; i < size; i++) {
		*(ptr + i) = str[i];
	}
	
	return ptr;
}
