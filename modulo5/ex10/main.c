#include <stdio.h>
#include <stdlib.h>
#include "new_str.h"

int main()
{
	char string[80] = "Teste String";
	
	char* ptr = new_str(string);
	printf("ESPERADO: %s\n", string);
	printf("OBTIDO:   %s\n", ptr);
	
	free(ptr);
	
	printf("\nPodemos reservar dinamicamente o espaco de memoria pretendido enquanto o programa executa, guardando o seu endereco num apontador para que o espaco possa ser acedido ao longo do programa, libertando sempre, no final, o espaço reservado.\n\n");
	return 0;
}	



