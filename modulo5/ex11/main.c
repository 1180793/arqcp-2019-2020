#include <stdio.h>
#include <stdlib.h>
#include "new_matrix.h"

int main(void){
	int linhas = 3;
	int colunas = 4;
	
	int** matriz = new_matrix(linhas, colunas);
	
	printf("Endereço: %p \n", matriz);
	printf("M[0][0] = %d\n", **matriz);	
	printf("M[1][2] = %d\n", *(matriz[1] + 2));
	printf("M[1][3] = %d\n", matriz[1][3]);
	printf("M[2][3] = %d\n", *(*(matriz + 2) + 3));
	
	int i;
	for (i = 0; i < linhas ; i++) {
		free (*(matriz + i));
	}
	free(matriz);

	return 0;
}
