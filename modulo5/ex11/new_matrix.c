#include <stdio.h>
#include <stdlib.h>

int **new_matrix(int lines, int columns) {
	
	int **m = (int**) calloc(lines, sizeof(int*)); //coluna de apontadores para cada linha
	if (m == NULL){
		printf ("Error reserving memory .\n");
		return NULL;
	}
	
	int i;
	for (i = 0; i < lines ; i++) {
		*(m + i) = (int *) calloc (columns , sizeof(int)); // Note : *(m+i) same as m[i]
		if(*(m + i) == NULL ){
			printf ("Error reserving memory .\n"); 
			return NULL;
		}
	}
	
	return m;
}
