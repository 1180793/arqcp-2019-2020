.section .data
	.equ DATA_SIZE, 244
	
	.equ AGE_OFFSET, 0 			# char age			- 1 byte
	.equ NUMBER_OFFSET, 2		# short number 		- 2 bytes
	.equ GRADES_OFFSET, 4		# int grades[10] 	- 10 x 4 bytes
	.equ NAME_OFFSET, 44		# char name[80]		- 80 x 1 byte
	.equ ADDRESS_OFFSET, 124	# char address[120] - 120 x 1 byte
	
.section .text
	.global update_grades # void update_grades(Student *s, int *new_grades)
	
update_grades:
	# prologue
	pushl %ebp 		# save previous stack frame pointer
	movl %esp, %ebp # the stack frame pointer for sum function
	pushl %ebx
	pushl %edi
	pushl %esi
	
	movl 8(%ebp), %esi			# ptrStudent -> %esi
	movl 12(%ebp), %edi			# ptrNewGrades -> %edi
	
	movl $0, %eax 				#contador
    addl $GRADES_OFFSET, %esi	# apontador s->grades
	
	vec_loop:
		cmpl $10, %ecx
		jge end
		
		movl (%edi, %ecx, 4), %edx
		movl %edx, (%esi, %ecx, 4)
		
		incl %ecx
		jmp vec_loop

end:
	popl %esi
	popl %edi
	popl %ebx
	# epilogue
	movl %ebp, %esp # restore the previous stack pointer ("clear" the stack)
	popl %ebp 		# restore the previous stack frame pointer
	ret
