#include <stdio.h>
#include "student.h"
#include "fill_student.h"
#include "update_grades.h"

int main() {
	
	Student st;
	
	Student *s = &st;
	fill_student(s, 19, 118, "Fernando", "Rua 0"); 
	printf("%d\n", s->age);
    printf("%d\n", s->number);
    printf("%s\n", s->name);
    printf("%s\n", s->address);
	
	printf("Notas Iniciais: ");
	int i;
	for (i = 0; i < 10; i++) {
		s->grades[i] = 0;
		printf("%d, ", s->grades[i]);
	}
	printf("\nNotas Finais: ");
	int newGrades[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	update_grades(s, newGrades);
	for (i = 0; i < 10; i++) {
		printf("%d, ", s->grades[i]);
	}
	printf("\n");
	
	return 0;
}	