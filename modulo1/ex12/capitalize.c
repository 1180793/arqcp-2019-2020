#include <stdio.h>
#include <string.h>

void capitalize (char *str) {
	int index = 0, string_size = 0, hasFinished = 0;
	while(hasFinished == 0) {
		 if(str[index] != '\0') {
			 string_size++;
		 }
		 else {
			 hasFinished = 1;
		 }
		 index++;
	 }
	 int i;
	 for (i = 0; i < string_size; i++) {
		 if(i == 0) {
			 if (*(str + i) >= 'a' && *(str + i) <= 'z') {
				*(str + i) = *(str + i) - 32;
			 }
		 }
		 if(*(str + i) == ' ') {
			 if (*(str + (i + 1)) >= 'a' && *(str + (i + 1)) <= 'z') {
				*(str + (i + 1)) = *(str + (i + 1)) - 32;
			 }
		 }
	 }
}
