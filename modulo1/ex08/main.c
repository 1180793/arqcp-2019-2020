#include <stdio.h>
#include "array_sort2.h"

int main(void) {
	
	int vec[7] = {5,4,2,1,0,8,9};
	int i, num = 7;
	printf("Unsorted Array: ");
	for (i = 0; i < num; i++) {
		printf("%d, ",vec[i]);
	}
	
	array_sort2(vec, num);
	
	printf("\nSorted Array: ");
	for (i = 0; i < num; i++) {
		printf("%d, ",vec[i]);
	}
	printf("\n");
	
	return 0;
}

	
	

