#include <stdio.h>
#include <string.h>

void frequencies(float *grades, int n, int *freq) {
	int i, nFreq;
	for(i = 0; i < 21; i++) {
		*(freq + i) = 0;
	}
	for (i = 0; i < n; i++) {
		nFreq = (int) *(grades + i);
		*(freq + nFreq) += 1;
	}
}
