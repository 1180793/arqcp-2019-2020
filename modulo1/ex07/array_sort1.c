#include <stdio.h>

void array_sort1(int *vec,int n) {
	int aux;
	int i;
	for (i = 0; i < n ; i++) {
		aux = vec[i];
		int j;
		for (j = i; j < n ; j++) {
			if(aux > vec[j]) {
				vec[i] = vec[j];
				vec[j] = aux;
				aux = vec[i];
			}
		}	
	}	
}
