#include <stdio.h>
#include "upper1.h"

int main(void){
	
	char string[10]  = "aBCdEFgHi";
	printf("Original String: %s\n", string);
	
	upper1(string);
	printf("Final String: %s\n", string);
		 
	return 0;
}
