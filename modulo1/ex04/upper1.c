#include <stdio.h>

void upper1(char *str) {
	
	int i = 0;
	char ch;
	
	while (*(str + i) != '\0') {
		ch = *(str + i);
		
		if (ch >= 'a' && ch <= 'z') {
			*(str + i) = *(str + i) - 32;
		}
		
		i++;  
	}
}
