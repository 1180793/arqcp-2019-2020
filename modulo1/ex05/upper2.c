#include <stdio.h>

void upper2(char *str) {
	
	char ch;
	
	while ((*str) != '\0') {
		
		ch = *str;
		
		if (ch >= 'a' && ch <= 'z') {
			*str = *str - 32;
		}
		str++;  
	}
	
}
