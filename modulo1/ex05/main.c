#include <stdio.h>
#include "upper2.h"

int main(void){
	
	char string[10]  = "aBCdEFgHi";
	printf("Original String: %s\n", string);
	
	upper2(string);
	printf("Final String: %s\n", string);
		 
	return 0;
}
