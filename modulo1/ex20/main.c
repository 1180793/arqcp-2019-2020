#include <stdio.h>
#include "find_all_words.h"

int main() {
	
	char str[] = {"googlegooglegoogle"};
	char word[] = {"go"};
	char* vec[20] = {NULL};
	
	printf("STRING ADDRESS: %p\n\n", str);
	
	find_all_words(str, word, vec);
	int i = 0;
	while(vec[i] != NULL) {
		printf("FOUND AT: %p\n", vec[i]);
		i++;
	}
	
	return 0;
}