#include <stdio.h>
#include <ctype.h>

char* find_word(char *str, char *word, char *initial_addr) {
	
	if(*word == '\0') {
		return NULL;
	}
	
	char *string = initial_addr;
	char *word_start = word;
	char *found_addr = initial_addr;
	
	while(*string != '\0' && *word != '\0') {
			if(toupper(*string) == toupper(*word)) {
				string++;
				word++;
			} else {
			    string++;
			    found_addr = string;
				word = word_start;
				
			}
				
	}
	
	word = word_start;
	
	if(*string == '\0') {
	   return NULL;
    }
	
	return found_addr;
    	
}	
