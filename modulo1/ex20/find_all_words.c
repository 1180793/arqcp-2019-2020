#include <stdio.h>
#include "find_word.h"

void find_all_words(char *str, char *word, char **addrs) {
	
	char *initial_addr = str;
	char *found;
	int i = 0;
	
	int flag = 0;
	while (flag == 0) {
		found = find_word(str, word, initial_addr);
		if (found == NULL) {
			flag = 1;
		} else {
			*(addrs + i) = found;
			i++;
			initial_addr = found + 1;
		}
	}
    	
}
