#include <stdio.h>
#include "sort_without_reps.h"

int main(void) {
	
	int array1[9] = {1, 2, 1, 4, 3, 3, 6, 5, 5};
	int array2[9];
	int num = 9;
	
	int i;
	printf("Array1: ");
	for (i = 0; i < num; i++) {
		printf("%d, ",array1[i]);
	}
	
	int size = sort_without_reps(array1, num, array2);
	
	printf("\nArray2: ");
	for (i = 0; i < size; i++) {
		printf("%d, ",array2[i]);
	}
	
	printf("\n");
	
	return 0;	
}
