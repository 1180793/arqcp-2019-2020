#include <stdio.h>

int sort_without_reps(int *src, int n, int *dest) {
	int i, j, aux;
	for (i = 0; i < n; i++) {
		for (j = i + 1; j < n; j++) {
			if (*(src + j) < *(src + i)) {
				aux = *(src + i); 
				*(src + i) = *(src + j); 
				*(src + j) = aux; 
			}
		}
	}
	
	j = 0;
	if (n == 1) {
		*(dest + j) = src[0];
		j++;
	} else {
		for (i = 1; i < n; i++) {
			aux = src[i - 1];
			if (aux != src[i]) {
				*(dest + j) = aux;
				j++;
				if (i == (n - 1)) {
					*(dest + j) = src[i];
					j++;
				}
			}
		}
	}
	
	return j;	
}
