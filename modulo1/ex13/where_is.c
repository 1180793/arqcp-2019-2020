#include <stdio.h>

int where_is(char *string,char c,int *p) {
	
	int where_is = 0,string_size = 0,cycle = 0;

	while(cycle==0) {
		if(string[where_is]!='\0') {
			string_size++;
		} else{
			cycle=1;
		}
		where_is++;
	}
	
	int vecIndex=0;
	int i;
	
	for (i = 0; i < string_size; i++) {
		if(*(string + i) == c) {
			p[vecIndex] = i;
			vecIndex++;
		}
	}
	
	return vecIndex; 
}
