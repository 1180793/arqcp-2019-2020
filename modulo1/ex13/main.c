#include <stdio.h>
#include "where_is.h"

int main(void)
{
	char string[15] ={"curto circuito"};
	int vec[14];
	int n = where_is(string, 'c', vec);
	
	printf("String: '%s'", string);
	printf("\nLetter C found on indexes: ");
	int i;
	for (i = 0; i < n; i++) {
		printf("%d, ", vec[i]);
	}
	printf("\nThere were found %d occurences of the letter 'c'.\n", n);
	
	return 0;
}
