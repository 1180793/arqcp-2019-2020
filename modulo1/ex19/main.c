#include <stdio.h>
#include "find_word.h"

int main() {
	
	char str[] = "john mike louis";
	char word[] = "mike";
	
	printf("STRING: %s\n", str);
	printf("WORD: %s\n", word);
	
	char *found = find_word(str, word, str);
	if (found == NULL) {
		printf("\nWord not found.");
	} else {
		printf("\nWord found at address: %p.\n\n", found);
	}
	
	return 0;
}