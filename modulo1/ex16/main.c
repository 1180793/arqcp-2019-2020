#include <stdio.h>
#include "where_exists.h"

int main(void){
	
	char str1[] = "xrt";
	char str2[] = "aarrxrt X";
	
	char *p1;
	p1 = str1;
	
	char *p2;
	p2 = str2;

	char *ptr = where_exists(p1, p2);
	printf("STRING 1: %s\n", str1);
	printf("STRING 2: %s\n", str2);
	
	if (ptr == NULL) {
		printf("\nSubstring not found!\n");
	} else {
		printf("\nSubstring found at address: %p\n", ptr);
	}
	
	return 0;
}
