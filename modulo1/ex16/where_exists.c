#include <stdio.h>
#include <string.h>

char* where_exists(char* str1, char* str2) {
	
	char *p1;
	p1 = str1;
	char *p2;
	int equal;
	
	while(*str2 != '\0') {
		p2 = str2;
		if(*str1 == *str2) {
			equal = 1;
			str1++;
			str2++;
			if(*str2 == '\0') {
				equal = 0;
			} else {	
				while(*str1 != '\0' && *str2 != '\0') {
					if(*str1 != *str2) {
						equal = 0;
					} else {
						equal = 1;
					}	
					str1++;
					str2++;				
				}
				if(equal == 1) {			
					return p2;
				}
			} 
		}
		str1 = p1;
		str2 = p2;
		str2++;		
	}	
	
	return NULL;
}
