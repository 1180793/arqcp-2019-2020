#include <stdio.h>
#include "compress_shorts.h"

int main(void){
	int num = 10;
	int n = num / 2;
	
	short shorts[] = {1, 3, 4, 5, 6, 7, 8, 9, 12, 43};
	int integers[n];
	
	compress_shorts(shorts, num, integers);
	
	int i;
	printf("SHORT VEC: ");
	for(i = 0; i < num; i++) {
		printf("0x%04X, ", shorts[i]);
	}
	printf("\nINTEGER VEC: ");
	for(i = 0; i < n; i++) {
		printf("0x%08X, ", integers[i]);
	}
	
	printf("\n");
	
	return 0;
}
