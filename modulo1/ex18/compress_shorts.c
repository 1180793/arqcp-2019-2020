#include <stdio.h>

void compress_shorts(short* shorts, int n_shorts, int* integers) {
	int nInts = n_shorts / 2;
	
	int i;
	for(i = 0; i < nInts; i++) {
		*(integers + i) = *((int*) (shorts + (2 * i)));
	}
}
