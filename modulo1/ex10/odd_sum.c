#include <stdio.h>

int odd_sum(int *p) {
	int i, soma = 0;
    int n = *p;
	
    for (i = 1; i < n + 1; i++) {
		if ((*(p + i) % 2) != 0) {
			soma += *(p + i);
		}
	}
    return soma;
}
