#include <stdio.h>
#include "odd_sum.h"

int main(void){
    int vec[] = {6, 2, 5, 1, 10, 3, 2};
	int i;
	
	printf("VEC: ");
	for (i = 1; i < 7; i++) {
		printf("%d, ", vec[i]);
	}
    int n = odd_sum(vec);
    printf("\nSoma Impares: %d\n", n);
    return 0;
}
