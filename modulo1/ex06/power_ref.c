#include <stdio.h>

void power_ref(int* x, int y) {

	int valorX = 1;
	int i = 0;
	
	while(i < y) {
		valorX = (valorX) * (*x);
		i++;
	}
	*x = valorX;
	if (y < 0) {
		*x = 0;
	}
	
	printf("Valor Final: %d\n",*x);
}
