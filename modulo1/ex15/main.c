#include <stdio.h>
#include "populate.h"
#include "check.h"
#include "inc_nsets.h"

int n = 0;

int main(void) {
	
	int num = 100;
	int vec[num];
	int limit = 20;
	
	int *p;
	p = vec;
	populate(p, num, limit);
	
	printf("Random Vec: ");
	int i;
	for (i = 0; i < num; i++) {
		printf("%d, ", vec[i]);
	}
	
	int x, y, z;
	
	for(i = 0; i < num - 3; i++) {
		x = *(p + i);
		y = *(p + (i + 1));
		z = *(p + (i + 2));
		
		if(check(x, y, z) == 1) {
			inc_nsets();
		}
	}
	
	printf("\nNumber of Sets: %d\n", n);
	
	return 0;
}

