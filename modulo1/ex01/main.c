#include <stdio.h>

int main() {
	int x = 5;
	int* xPtr = &x;
	short y = *xPtr + 3;
	int vec[] = {10, 11, 12, 13};
	
	int i;

	printf("X: %i\n", x);
	printf("Y: %i\n", y);
	
	printf("Address(Hex) of X: %p\n", &x);
	printf("Address(Hex) of xPtr: %p\n", &xPtr);
	
	printf("Value pointed by xPtr: %d\n", *xPtr); 
	
	printf("Address of Vec: %p\n", &vec);
	
	 
	for (i = 0; i < sizeof(vec)/sizeof(vec[0]); i++)
	{
		printf("vec[%d]: %d\n", i, vec[i]);
	}
	
	for (i = 0; i < sizeof(vec)/sizeof(vec[0]); i++)
	{
		printf("Address of vec[%d]: %p\n", i, &vec[i]);
	}
	
	return 0;
}
/**
 * 1 b) O endereço de memoria de vec é o mesmo de vec[0], ocupando essa e as 3 posiçoes
 * seguintes, e os elementos seguintes do vetor com a mesma logica de 4 em 4 posiçoes 
 * de memoria 
 * 
 * 1 c) Diferentes **/


