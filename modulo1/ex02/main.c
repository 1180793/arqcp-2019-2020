#include <stdio.h>
#include "copy_vec.h"

int main(void) {
	int vec1[5];
	int vec2[5];
	vec1[0] = 1;
	vec1[1] = 2;
	vec1[2] = 3;
	vec1[3] = 4;
	vec1[4] = 5;
	int num = 5;
	
	int i;
	printf("VEC1: ");
	for(i = 0; i < num; i++) {
		printf("%d, ", vec1[i]);
	}
	
	copy_vec(vec1, vec2, num);
	
	printf("\nVEC2: ");
	for (i = 0; i < num; i++)	{
		printf("%d, ", vec2[i]);
	}		
	printf("\n");
	
	return 0;
}
