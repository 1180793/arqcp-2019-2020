#include <stdio.h>
#include "sum_even.h"

int sum_even(int *p, int num) {
	int sumEven = 0;
	int i = 0;

	while(i < num) {
		if (((*p) % 2) == 0) {
			sumEven = sumEven + (*p);
		}
		i++;
		p++;
	}

	return sumEven;
}
