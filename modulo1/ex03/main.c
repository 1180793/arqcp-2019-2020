#include <stdio.h>
#include "sum_even.h"

int main(void) {
	int num = 6;
	int vec[] = {0,5,4,3,2,6};
	int *p = vec;
	
	int i;
	printf("VEC: ");
	for(i = 0; i < num; i++) {
		printf("%d, ", vec[i]);
	}
	printf("\nSum Even: %i\n", sum_even(p, num));

	return 0;
}
