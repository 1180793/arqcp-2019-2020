#include <stdio.h>
#include "swap.h"

int main(void) {
	
	int num = 5;
	int vec1[] = {1, 2, 3, 4, 5};
	int vec2[] = {5, 6, 7, 8, 9};
	
	int i;
	printf("VEC1 ORIGINAL: ");
	for (i = 0; i < num; i++) {
		printf("%d, ", vec1[i]);
	}
	printf("\nVEC2 ORIGINAL: ");
	for (i = 0; i < num; i++) {
		printf("%d, ", vec2[i]);
	}
	
	swap(vec1, vec2, num);
	
	printf("\n\nVEC1 FINAL: ");
	for (i = 0; i < num; i++) {
		printf("%d, ", vec1[i]);
	}
	printf("\nVEC2 FINAL: ");
	for (i = 0; i < num; i++) {
		printf("%d, ", vec2[i]);
	}
	
	printf("\n");
	
	return 0;
}
