#include <stdio.h>
#include "palindrome.h"

int is_valid(char chr) {
	if ((chr >= '0' && chr <= '9') || (chr >= 'A' && chr <= 'Z') || (chr >= 'a' && chr <= 'z')) {
		return 1;
	}
	return 0;
}

int palindrome(char *str) {
	
	if(*str == '\0') {
		return 0;
	}
	
	if(*(str + 1) == '\0') {
		return 1;
	}
	
	int n = 0;
	while (*(str + n) != '\0') {
		n++;
	}
	
	int lower = 0;
	int higher = n - 1;
	
	char strCopy[n];
	n = 0;
	while (*(str + n) != '\0') {
		*(strCopy + n) = *( str + n);
		if (*(strCopy + n) >= 'a' && *(strCopy + n) <= 'z') {
			*(strCopy + n) = *(strCopy + n) - 32;
		}
		n++;
	}
	
	while (lower < higher) {
		char start = strCopy[lower];
		char end = strCopy[higher];
		
		if (!(is_valid(start))) {
			lower++;
		} else if (!(is_valid(end))) {
			higher--;
		} else if (start == end) {
			lower++;
			higher--;
		} else {
			return 0;
		}
	}
	
	return 1;
}
