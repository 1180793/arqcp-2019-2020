#include <stdio.h>
#include "palindrome.h"

int main() {
	char str1[] = "Never odd or even";
	char str2[] = "A man a plan a canal Panama.";
	char str3[] = "Gateman sees name, garageman sees name tag";
	char str4[] = "Test Palindrome";
	
	printf("STR1: %s\n", str1);
	if (palindrome(str1) == 1) {
		printf("STR1 is a palindrome.\n");
	} else if (palindrome(str1) == 0) {
		printf("STR1 is not a palindrome.\n");
	}
	
	printf("\nSTR2: %s\n", str2);
	if (palindrome(str2) == 1) {
		printf("STR2 is a palindrome.\n");
	} else if (palindrome(str2) == 0) {
		printf("STR2 is not a palindrome.\n");
	}
	
	printf("\nSTR3: %s\n", str3);
	if (palindrome(str3) == 1) {
		printf("STR3 is a palindrome.\n");
	} else if (palindrome(str3) == 0) {
		printf("STR3 is not a palindrome.\n");
	}
	
	printf("\nSTR4: %s\n", str4);
	if (palindrome(str4) == 1) {
		printf("STR4 is a palindrome.\n");
	} else if (palindrome(str4) == 0) {
		printf("STR4 is not a palindrome.\n");
	}

	return 0;
}
