#include <stdio.h>
#include "multiplication_table_n.h"

void multiplication_table(void) {
	int i;
	for(i = 1; i < 11; i++) {
		printf("\n");
		multiplication_table_n(i);
		printf("\n");
	}
}
