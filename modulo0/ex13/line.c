#include <stdio.h>

void line(int x, int y) {
	int res = x * y;
	printf("%d * %d = %d\n", x, y, res);
}