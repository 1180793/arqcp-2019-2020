#include <stdio.h>
#include "average.h"

int average(int n1, int n2){
	return (n1 + n2) / 2;
}

int average_array (int v [], int n) {
	int i;
	int total;
	for (i = 0; i < n; i++) {
		total = total + v[i];
	}
	return total / n;
}

int average_global_array() {
	int i;
	int total = 0;
	for (i = 0; i < g_n; i++) {
		total = total + g_v[i];
	}
	return total /g_n;
}