#include <stdio.h>
#include "count.h"

int main(void) {
	
	int vec[] = {1, 2, 3, 4, 2, 4, 3, 2};
	int n = 8;
	int value = 2;
	
	printf("VEC: ");
	int i;
	for (i = 0; i < n; i++) {
		printf("%d, ", vec[i]);
	}
	printf("\nThe number %d appears %d times in the vec.\n", value, count(vec, n, value));
	
	return 0;
}
