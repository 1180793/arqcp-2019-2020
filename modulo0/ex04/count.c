#include <stdio.h>
#include "count.h"

int count(int *vec, int n, int value) {
	int i;
	int x = 0;
	for (i = 0; i < n; i++) {
		if (*(vec + i) == value) {
			x = x + 1;
		}
	}
	return x;
}
