#include <stdio.h>

int main(void) {
	
	printf("char: %d bytes\n", sizeof(char));
	printf("int: %d bytes\n", sizeof(int));
	printf("unsigned int: %d bytes\n", sizeof(unsigned int));
	printf("long: %d bytes\n", sizeof(long));
	printf("short: %d bytes\n", sizeof(short));
	printf("long long: %d bytes\n", sizeof(long long));
	printf("float: %d bytes\n", sizeof(float));
	printf("double: %d bytes\n", sizeof(double));
	
	return 0;
	
}
