#include <stdio.h>
#include "count_words.h"

int main(void) {
	
	char str[] = "Hello, welcome to the store.";
	
	printf("\nSTRING: %s", str);
	
	int i = count_words(str);
	
	printf("\nWORDS: %d\n", i);
	
	return 0;
}
