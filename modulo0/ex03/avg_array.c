#include <stdio.h>

int avg_array(int* vec, int n){
    int i;
    int sum = 0;
    for(i = 0; i < n; i++){
        sum = sum + *(vec + i);
    }
    
    int average = sum / n;

    return average;
}
