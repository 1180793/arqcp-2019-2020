#include <stdio.h>
#include "fill_array.h"
#include "avg_array.h"

int main (void) {

	int vec[30];

    fill_array(vec);
    int average = avg_array(vec, 30);

    printf("\nAVERAGE: %d\n", average);

	return 0;

}


