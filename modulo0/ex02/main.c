#include <stdio.h>
#include "sum.h"

int main(void) {
	
	int res = 10, a = 0, b = 0;
	
	while (res >= 10) {
		printf("\nFirst Value: ");
		scanf("%d",&a);
	
		printf("Second Value: ");
		scanf("%d",&b);
		
		res = sum(a, b);
		
		if (res >= 10) {
			printf("\nThe sum of the entered values must be less than 10. Sum = %d\n", res);
		}
	}
	
	printf("\nThe sum of the entered values is %d\n", res);
	
	return 0;
}
