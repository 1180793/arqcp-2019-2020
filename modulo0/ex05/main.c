#include <stdio.h>
#include "string_to_int.h"

int main (void) {
	
	char str[] = "12345";
    int result = string_to_int("12345");
	
    printf("STRING: %s\nRESULT: %d\n", str, result);

	return 0;

}
