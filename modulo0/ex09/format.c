#include <stdio.h>
#include "format.h"

int is_valid(char chr) {
	if ((chr >= '0' && chr <= '9') || (chr >= 'a' && chr <= 'f') || (chr >= 'A' && chr <= 'F')) {
		return 1;
	}
	return 0;
}

int is_hex(char chr) {
	if ((chr >= 'A' && chr <= 'F') || (chr >= 'a' && chr <= 'f')) {
		return 1;
	}
	return 0;
}

int is_decimal(char chr) {
	if (chr >= '8' && chr <= '9') {
		return 1;
	}
	return 0;
}

int is_octal(char chr) {
	if (chr >= '2' && chr <= '7') {
		return 1;
	}
	return 0;
}

char* format(char* p) {
	
	int i = 0, valid = 1, hex = 0, dec = 0, oct = 0;
	
	while (*(p + i) != '\0') {
		char chr = *(p + i);
		if (!(is_valid(chr))) {
			valid = 0;
		} else if (is_hex(chr)) {
			hex = 1;
		} else if (is_decimal(chr)) {
			dec = 1;
		} else if (is_octal(chr)) {
			oct = 1;
		}
		i++;
	}
	
	if (valid == 1) {
		if (hex == 1) {
			return "Hexadecimal";
		} else if (dec == 1) {
			return "Decimal";
		} else if (oct == 1) {
			return "Octal";
		} else {
			return "Binary";
		}
	} else {
		return "Invalid Input";
	}
}

