#include <stdio.h>
#include "format.h"

int main() {
	
	char s[] = "010TYZ20ZF11";
	char t[] = "010101001011";
	char x[] = "010101201411";
	char y[] = "010101201911";
	char z[] = "0101A1201F11";
	
	printf("\nS: %s\n", s);
	printf("T: %s\n", t);
	printf("X: %s\n", x);
	printf("Y: %s\n", y);
	printf("Z: %s\n", z);

	printf("\nS is %s\n", format(s));
	printf("T is %s\n", format(t));
	printf("X is %s\n", format(x));
	printf("Y is %s\n", format(y));
	printf("Z is %s\n", format(z));
	
	return 0;
}
