#include <stdio.h>
#include "string_to_int.h"
#include "average.h"

int main (void) {
	
	char c1[10];
	char c2[10];
	
	printf("Enter the first number: ");
	scanf("%s", c1);
	printf("Enter the second number: ");
	scanf("%s", c2);
	
	int x = string_to_int(c1);
	int y = string_to_int(c2);
    int result = average(x, y);
	
    printf("STRING1: %s\nSTRING2: %s\n\nAVERAGE: %d\n", c1, c2, result);

	return 0;

}
