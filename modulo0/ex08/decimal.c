#include <stdio.h>
#include "decimal.h"

int integer_part(char* x) {
	int flag = 0;
	int n = 0;

	while (*x != '\0' && flag == 0) {
		if (*x == '.') {
			flag = 1;
		}
		n++;
		x++;
	}
	x = x - n;
	int integer = 0;
	int i;
	for (i = 0; i < n - 1; i++) {
		integer = integer * 10 + *(x + i) - '0';
	}
	return integer;
}

int fractional_part(char* x) {
	int j = 0;
	int n = 0;
	while (*x != '\0') {
		if (*x == '.') {
			n = 0;
		}
		j++;
		n++;
		x++;
	}
	x = x - j;
	int fractional = 0;
	int i;
	for (i = j - n + 1; i < j; i++) {
		fractional = fractional * 10 + *(x + i) - '0';
	}
	return fractional;
}
