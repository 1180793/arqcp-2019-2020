#include <stdio.h>
#include "decimal.h"

int main() {
	
	char x[10];
	printf("Enter the number: ");
	scanf("%s", x);
	printf("Integer Part: %i\n" , integer_part(x));
	printf("Fractional Part: %i\n" , fractional_part(x));

	return 0;
}
